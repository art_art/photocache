package com.art.rmr.photocache.controller.interfaces;

import com.art.rmr.photocache.model.transport.Media;

import java.io.IOException;
import java.util.List;

/**
 * Created by Артём on 25.07.2015.
 * Интерфейс получения новых фото.
 */
public interface ImagesAccumulator
{
    int addNewImages(List<Media> medias) throws IOException;

    void onImagesAdded(int addedImagesCount);
}
