package com.art.rmr.photocache.controller.click;

import android.content.Context;

import com.art.rmr.photocache.controller.storage.FavoritesStorage;

import java.io.IOException;

/**
 * Created by Артём on 26.07.2015.
 * Слушатель снятия отметки избранное по имени.
 */
public class RemoveByNameClickListener extends RemoveBaseClickListener
{
    private FavoritesStorage mFavoritesStorage;
    private String mName;

    public RemoveByNameClickListener(Context context, FavoritesStorage favoritesStorage, String name)
    {
        super(context);
        mFavoritesStorage = favoritesStorage;
        mName = name;
    }

    @Override
    protected void remove() throws IOException
    {
        mFavoritesStorage.removeImageFromFavorites(mName);
    }
}
