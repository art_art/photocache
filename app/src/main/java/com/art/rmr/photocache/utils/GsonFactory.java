package com.art.rmr.photocache.utils;

import com.art.rmr.photocache.controller.network.InstagramServerRequest;
import com.art.rmr.photocache.model.transport.Media;
import com.art.rmr.photocache.model.transport.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by Артём on 19.07.2015.
 */
public class GsonFactory
{
    private static GsonBuilder sGsonBuilder;

    private static GsonBuilder getBuilder()
    {
        if (sGsonBuilder == null)
        {
            sGsonBuilder = new GsonBuilder();
            sGsonBuilder.registerTypeAdapter(User.class, new User.UserDeserializer());
            sGsonBuilder.registerTypeAdapter(Media.Image.class, new Media.Image.ImageDeserializer());
            sGsonBuilder.registerTypeAdapter(Media.class, new Media.MediaDeserializer());
            sGsonBuilder.registerTypeAdapter(InstagramServerRequest.InstagramJson.class, new InstagramServerRequest.InstagramJson.IJDeserializer());
        }
        return sGsonBuilder;
    }

    public static Gson createGson()
    {
        return getBuilder().create();
    }
}
