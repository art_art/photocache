package com.art.rmr.photocache.controller.storage;

import android.accounts.Account;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.art.rmr.photocache.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Артём on 25.07.2015.
 * Реализация хранилища фото.
 */
public class FavoritesFileStorage extends FavoritesStorage
{
    private Resources mResources;
    private File mStorage;
    private boolean mStorageExists;

    public FavoritesFileStorage(@NonNull Context context, @NonNull Account account)
    {
        mResources = context.getResources();
        mStorage = new File(new File(context.getFilesDir(), "images"), account.name);
        mStorageExists = mStorage.exists();
    }

    private static String getUri(File file)
    {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("file");
        builder.path(file.getPath());
        return builder.build().toString();
    }

    private synchronized void assertStorageExists()
    {
        if (!mStorageExists)
        {
            throw new IllegalStateException();
        }
    }

    @Override
    public int getFavoritesCount()
    {
        synchronized (this)
        {
            if (!mStorageExists)
            {
                return 0;
            }
        }
        return mStorage.list().length;
    }

    @Override
    public FavoriteInfo getFavoriteImage(int position)
    {
        assertStorageExists();
        String name = mStorage.list()[position];
        File imageFile = new File(mStorage, name);
        return new FavoriteInfo(getUri(imageFile), position, name);
    }

    @Override
    public FavoriteInfo isImageFavorite(String name)
    {
        synchronized (this)
        {
            if (!mStorageExists)
            {
                return new FavoriteInfo();
            }
        }

        File image = new File(mStorage, name);
        if (image.exists())
        {
            return new FavoriteInfo(getUri(image), getPosition(name), name);
        }
        else
        {
            return new FavoriteInfo();
        }
    }

    @Override
    protected FavoriteInfo saveImage(String name, Bitmap image) throws IOException
    {
        synchronized (this)
        {
            if (!mStorageExists)
            {
                if (!mStorage.mkdirs())
                {
                    String message = mResources.getString(R.string.message_creating_directory_failed);
                    throw new IOException(message);
                }
                mStorageExists = true;
            }
        }

        final File imageFile = new File(mStorage, name);
        image.compress(Bitmap.CompressFormat.JPEG, 90, new FileOutputStream(imageFile));

        return new FavoriteInfo(getUri(imageFile), getPosition(name), name);
    }

    private int getPosition(String name)
    {
        String[] files = mStorage.list();
        int position = 0;
        for (; position < files.length; ++position)
        {
            if (name.compareTo(files[position]) == 0)
            {
                return position;
            }
        }
        throw new IllegalStateException();
    }

    @Override
    protected String remove(int position) throws IOException
    {
        assertStorageExists();
        String[] files = mStorage.list();
        String name = files[position];
        File image = new File(mStorage, name);
        if (!image.delete())
        {
            throw new IOException(mResources.getString(R.string.message_deleting_file_failed));
        }
        return name;
    }

    @Override
    protected int remove(String name) throws IOException
    {
        assertStorageExists();
        File image = new File(mStorage, name);
        if (!image.exists())
        {
            throw new IllegalStateException();
        }
        int position = getPosition(name);
        if (!image.delete())
        {
            throw new IOException(mResources.getString(R.string.message_deleting_file_failed));
        }
        return position;
    }
}
