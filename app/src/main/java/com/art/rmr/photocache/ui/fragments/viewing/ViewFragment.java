package com.art.rmr.photocache.ui.fragments.viewing;

import android.os.Bundle;

import com.art.rmr.photocache.R;

/**
 * Created by Артём on 28.07.2015.
 * Фрагмент просмотра фото.
 */
public class ViewFragment extends BaseViewFragment
{
    public ViewFragment()
    {
    }

    public static ViewFragment newInstance(String imageUri)
    {
        ViewFragment fragment = new ViewFragment();
        Bundle args = new Bundle();
        args.putString(ARG_IMAGE_URI, imageUri);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId()
    {
        return R.layout.fragment_view_image;
    }

    public void updateImage(String imageUri)
    {
        setImage(imageUri);
    }
}
