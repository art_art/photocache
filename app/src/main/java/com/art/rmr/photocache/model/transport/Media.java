package com.art.rmr.photocache.model.transport;

import android.support.annotation.Nullable;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by art090390 on 14.07.2015.
 */
public class Media extends Entity
{
    public static class Image
    {
        public static class ImageDeserializer implements JsonDeserializer<Image>
        {
            @Override
            public Image deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException
            {
                Image image = new Image();
                JsonObject imageObject = json.getAsJsonObject();
                image.mUrl = imageObject.get(FIELD_URL).getAsString();
                image.mWidth = imageObject.get(FIELD_WIDTH).getAsInt();
                image.mHeight = imageObject.get(FIELD_HEIGHT).getAsInt();
                return image;
            }
        }

        private static final String FIELD_URL = "url";
        private static final String FIELD_WIDTH = "width";
        private static final String FIELD_HEIGHT = "height";

        private int mWidth;
        private int mHeight;
        private String mUrl;

        private Image()
        {
        }

        public String getUrl()
        {
            return mUrl;
        }
    }

    public static class MediaDeserializer implements JsonDeserializer<Media>
    {
        @Override
        public Media deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException
        {
            return new Media(json.getAsJsonObject(), context);
        }
    }

    private static final String FIELD_CAPTION = "caption";
    private static final String FIELD_CAPTION_TEXT = "text";
    private static final String FIELD_LINK = "link";
    private static final String FIELD_LIKES = "likes";
    private static final String FIELD_LIKES_COUNT = "count";
    private static final String FIELD_CREATED_TIME = "created_time";
    private static final String FIELD_IMAGES = "images";
    private static final String FIELD_IMAGES_LOW_RESOLUTION = "low_resolution";
    private static final String FIELD_IMAGES_THUMBNAIL = "thumbnail";
    private static final String FIELD_IMAGES_STANDART_RESOLUTION = "standard_resolution";

    @Nullable
    private String mTitle;
    private String mLink;
    private int mLikes;
    private long mCreatedTime;
    private Image mThumbnail;
    private Image mSmallImage;
    private Image mImage;

    private Media(JsonObject mediaObject, JsonDeserializationContext context)
    {
        super(mediaObject);
        JsonElement e = mediaObject.get(FIELD_CAPTION);
        if (e != null)
        {
            mTitle = e.getAsJsonObject().get(FIELD_CAPTION_TEXT).getAsString();
        }
        mLink = mediaObject.get(FIELD_LINK).getAsString();
        e = mediaObject.get(FIELD_LIKES);
        if (e != null)
        {
            mLikes = e.getAsJsonObject().get(FIELD_LIKES_COUNT).getAsInt();
        }
        mCreatedTime = mediaObject.get(FIELD_CREATED_TIME).getAsLong();
        JsonObject images = mediaObject.getAsJsonObject(FIELD_IMAGES);
        mThumbnail = context.deserialize(images.get(FIELD_IMAGES_THUMBNAIL), Image.class);
        mSmallImage = context.deserialize(images.get(FIELD_IMAGES_LOW_RESOLUTION), Image.class);
        mImage = context.deserialize(images.get(FIELD_IMAGES_STANDART_RESOLUTION), Image.class);
    }

    public String getImageUrl()
    {
        return mImage.getUrl();
    }
}
