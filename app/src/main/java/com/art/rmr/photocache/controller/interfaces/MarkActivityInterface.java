package com.art.rmr.photocache.controller.interfaces;

/**
 * Created by art090390 on 28.07.2015.
 * Интерфейс для активити просмотра фото.
 */
public interface MarkActivityInterface
{
    void marked(boolean wasFavorite);
}
