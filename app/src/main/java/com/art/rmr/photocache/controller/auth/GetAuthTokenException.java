package com.art.rmr.photocache.controller.auth;

/**
 * Created by Артём on 25.07.2015.
 */
public class GetAuthTokenException extends Exception
{
    public GetAuthTokenException(String message)
    {
        super(message);
    }
}
