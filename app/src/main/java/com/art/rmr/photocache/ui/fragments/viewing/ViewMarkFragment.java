package com.art.rmr.photocache.ui.fragments.viewing;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.art.rmr.photocache.R;
import com.art.rmr.photocache.controller.PaletteBroadcastReceiver;
import com.art.rmr.photocache.controller.interfaces.MarkActivityInterface;
import com.art.rmr.photocache.ui.views.TouchImageView;
import com.art.rmr.photocache.utils.ViewColorAnimator;

/**
 * Created by art090390 on 28.07.2015.
 * Фрагмент просмотра фото с кнопкой избранного.
 */
public class ViewMarkFragment extends BaseViewFragment implements View.OnClickListener, TouchImageView.OnTouchImageViewListener
{
    private static final String ARG_IS_FAVORITE = "ARG_IS_FAVORITE";
    private static final String ARG_FAB_COLOR = "ARG_FAB_COLOR";

    private MarkActivityInterface mActivityInterface;
    private Integer mFabColor;
    private TouchImageView mImageView;
    private boolean mImageZoomed = false;
    private FloatingActionButton mFab;
    private boolean mIsFavorite;
    private BroadcastReceiver mPaletteReceiver;

    public ViewMarkFragment()
    {

    }

    public static ViewMarkFragment newInstance(String imageUri, boolean isFavorite)
    {
        ViewMarkFragment fragment = new ViewMarkFragment();
        Bundle args = new Bundle();
        args.putString(ARG_IMAGE_URI, imageUri);
        args.putBoolean(ARG_IS_FAVORITE, isFavorite);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        try
        {
            mActivityInterface = (MarkActivityInterface)activity;
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString()
                    + " must implement MarkActivityInterface");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Bundle args = savedInstanceState == null ? getArguments() : savedInstanceState;
        mIsFavorite = args.getBoolean(ARG_IS_FAVORITE);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);

        mImageView = (TouchImageView)rootView.findViewById(R.id.image);
        mImageView.setOnTouchImageViewListener(this);

        mFab = (FloatingActionButton)rootView.findViewById(R.id.fab);
        mFab.setImageResource(mIsFavorite ? R.mipmap.ic_white_star_outlined : R.mipmap.ic_white_star_filled);
        mFab.setOnClickListener(this);
        if (savedInstanceState != null && savedInstanceState.containsKey(ARG_FAB_COLOR))
        {
            mFabColor = savedInstanceState.getInt(ARG_FAB_COLOR);
            setFabColor(mFabColor);
        }

        return rootView;
    }

    @Override
    public void onStart()
    {
        super.onStart();

        mPaletteReceiver = new PaletteBroadcastReceiver(getActivity())
        {
            @Override
            protected void onColorsReceived(int primary, int primaryDark, int accent)
            {
                int oldFabColor = mFabColor == null ? getResources().getColor(R.color.color_accent) : mFabColor;
                mFabColor = accent;
                new ViewColorAnimator(oldFabColor, mFabColor)
                {
                    @Override
                    protected void changeColor(int color)
                    {
                        setFabColor(color);
                    }
                }.animate();
            }
        };
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mPaletteReceiver, PaletteBroadcastReceiver.sPaletteFilter);
    }

    @Override
    public void onStop()
    {
        super.onStop();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mPaletteReceiver);
        mPaletteReceiver = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putBoolean(ARG_IS_FAVORITE, mIsFavorite);
        if (mFabColor != null)
        {
            outState.putInt(ARG_FAB_COLOR, mFabColor);
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        mActivityInterface = null;
    }


    @Override
    protected int getLayoutId()
    {
        return R.layout.fragment_view_mark_image;
    }

    private void setFabColor(int color)
    {
        ColorStateList list = new ColorStateList(
                new int[][] {new int[] {}},
                new int[] {color});
        mFab.setBackgroundTintList(list);
    }

    @Override
    public void onClick(View v)
    {
        mActivityInterface.marked(mIsFavorite);
    }

    @Override
    public void onMove()
    {
        boolean isImageZoomed = mImageView.isZoomed();
        if (mImageZoomed)
        {
            if (!isImageZoomed)
            {
                mFab.show();
            }
        }
        else
        {
            if (isImageZoomed)
            {
                mFab.hide();
            }
        }
        mImageZoomed = isImageZoomed;
    }
}
