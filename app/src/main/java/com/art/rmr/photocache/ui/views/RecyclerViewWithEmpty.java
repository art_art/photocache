package com.art.rmr.photocache.ui.views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Артём on 25.07.2015.
 */
public class RecyclerViewWithEmpty extends RecyclerView
{
    private View mEmptyView;
    private final AdapterDataObserver mDataObserver = new AdapterDataObserver()
    {
        @Override
        public void onChanged()
        {
            checkIsEmpty();
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount)
        {
            checkIsEmpty();
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount)
        {
            checkIsEmpty();
        }
    };

    public RecyclerViewWithEmpty(Context context)
    {
        super(context);
    }

    public RecyclerViewWithEmpty(Context context, @Nullable AttributeSet attrs)
    {
        super(context, attrs);
    }

    public RecyclerViewWithEmpty(Context context, @Nullable AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
    }

    @Override
    public void setAdapter(Adapter adapter)
    {
        final Adapter oldAdapter = getAdapter();
        if (oldAdapter != null)
        {
            oldAdapter.unregisterAdapterDataObserver(mDataObserver);
        }

        super.setAdapter(adapter);
        if (adapter != null)
        {
            adapter.registerAdapterDataObserver(mDataObserver);
        }

        checkIsEmpty();
    }

    private boolean isEmpty()
    {
        Adapter adapter = getAdapter();
        return adapter == null || adapter.getItemCount() == 0;
    }

    private void checkIsEmpty()
    {
        boolean showEmpty = isEmpty();
        if (mEmptyView != null)
        {
            mEmptyView.setVisibility(showEmpty ? VISIBLE : GONE);
        }
    }

    public void setEmptyView(View emptyView)
    {
        mEmptyView = emptyView;
        checkIsEmpty();
    }
}
