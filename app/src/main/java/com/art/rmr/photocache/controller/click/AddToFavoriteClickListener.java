package com.art.rmr.photocache.controller.click;

import android.content.Context;
import android.view.View;

import com.art.rmr.photocache.controller.storage.FavoritesStorage;

/**
 * Created by Артём on 26.07.2015.
 * Слушатель установки отметки избранное.
 */
public class AddToFavoriteClickListener implements View.OnClickListener
{
    private FavoritesStorage mFavoritesStorage;
    private Context mContext;
    private String mName;
    private String mUrl;

    public AddToFavoriteClickListener(Context context, FavoritesStorage favoritesStorage, String name, String url)
    {
        mFavoritesStorage = favoritesStorage;
        mContext = context;
        mName = name;
        mUrl = url;
    }

    @Override
    public void onClick(final View v)
    {
        new FavoritesStorage.SaveImageTask(mContext, mFavoritesStorage, v, mName, mUrl).execute();
    }
}
