package com.art.rmr.photocache.utils;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;

/**
 * Created by art090390 on 28.07.2015.
 * Класс для анимации цвета.
 */
public abstract class ViewColorAnimator implements ValueAnimator.AnimatorUpdateListener
{
    private ValueAnimator mColorAnimation;

    protected ViewColorAnimator(int colorFrom, int colorTo)
    {
        mColorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
        mColorAnimation.addUpdateListener(this);
        mColorAnimation.setDuration(500);
    }

    public void animate()
    {
        mColorAnimation.start();
    }

    protected abstract void changeColor(int color);

    @Override
    public final void onAnimationUpdate(ValueAnimator animation)
    {
        changeColor((Integer)animation.getAnimatedValue());
    }
}
