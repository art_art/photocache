package com.art.rmr.photocache.controller.implementations.instagram;

import com.art.rmr.photocache.controller.adapters.MediaAdapter;
import com.art.rmr.photocache.controller.interfaces.FeedActivityInterface;
import com.art.rmr.photocache.controller.interfaces.ImagesAccumulator;
import com.art.rmr.photocache.controller.storage.FavoritesStorage;
import com.art.rmr.photocache.model.transport.Media;
import com.art.rmr.photocache.model.ui.DisplayMedia;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Артём on 27.07.2015.
 * Реализация интерфейса для обновления фото.
 */
public class InstagramImagesAccumulator implements ImagesAccumulator
{
    private MediaAdapter mAdapter;
    private InstagramFeedData mFeedData;
    private FeedActivityInterface mActivityInterface;

    public InstagramImagesAccumulator(MediaAdapter adapter, InstagramFeedData feedData, FeedActivityInterface activityInterface)
    {
        mAdapter = adapter;
        mFeedData = feedData;
        mActivityInterface = activityInterface;
    }

    @Override
    public int addNewImages(List<Media> medias) throws IOException
    {
        Set<String> oldImages = new HashSet<>();
        for (int i = 0; i < mFeedData.getImagesCount(); ++i)
        {
            oldImages.add(mFeedData.getImage(i).getName());
        }

        List<DisplayMedia> newImages = new ArrayList<>();
        for (Media image : medias)
        {
            String name = image.getId();
            if (!oldImages.contains(name))
            {
                FavoritesStorage.FavoriteInfo info = mActivityInterface.getFavoritesStorage().isImageFavorite(name);
                DisplayMedia media;
                if (info.isFavorite())
                {
                    media = new DisplayMedia(name, image.getImageUrl(), info.getUri());
                }
                else
                {
                    media = new DisplayMedia(name, image.getImageUrl());
                }
                newImages.add(media);
            }
        }

        if (!newImages.isEmpty())
        {
            mFeedData.addImages(newImages);
        }

        return newImages.size();
    }

    @Override
    public void onImagesAdded(int addedImagesCount)
    {
        int oldCount = mFeedData.getImagesCount();
        mAdapter.notifyItemRangeInserted(oldCount - addedImagesCount, addedImagesCount);
    }
}
