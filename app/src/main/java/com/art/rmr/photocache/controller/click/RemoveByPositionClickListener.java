package com.art.rmr.photocache.controller.click;

import android.content.Context;

import com.art.rmr.photocache.controller.storage.FavoritesStorage;

import java.io.IOException;

/**
 * Created by Артём on 26.07.2015.
 * Слушатель нажатия удаления из избранных по позиции.
 */
public class RemoveByPositionClickListener extends RemoveBaseClickListener
{
    private FavoritesStorage mFavoritesStorage;
    private int mPosition;

    public RemoveByPositionClickListener(Context context, int position, FavoritesStorage favoritesStorage)
    {
        super(context);
        mPosition = position;
        mFavoritesStorage = favoritesStorage;
    }

    @Override
    protected void remove() throws IOException
    {
        mFavoritesStorage.removeImageFromFavorites(mPosition);
    }
}
