package com.art.rmr.photocache.controller.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.art.rmr.photocache.R;
import com.art.rmr.photocache.controller.interfaces.FeedImagesData;
import com.squareup.picasso.Picasso;

/**
 * Created by Артём on 25.07.2015.
 * Базовый класс адаптер фото.
 */
public final class MediaAdapter extends RecyclerView.Adapter<MediaViewHolder>
{
    private Picasso mPicasso;
    private LayoutInflater mLayoutInflater;
    private FeedImagesData mFeedImagesData;

    public MediaAdapter(Context context, FeedImagesData feedImagesData)
    {
        mPicasso = Picasso.with(context);
        mLayoutInflater = LayoutInflater.from(context);
        mFeedImagesData = feedImagesData;
    }

    @Override
    public final MediaViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View mediaView = mLayoutInflater.inflate(R.layout.media, parent, false);
        return new MediaViewHolder(mediaView);
    }

    @Override
    public void onBindViewHolder(MediaViewHolder holder, int position)
    {
        ImageButton favoriteButton = holder.getFavoriteButton();
        ImageView imageView = holder.getImage();
        mPicasso.load(mFeedImagesData.getImageUri(position)).fit().into(imageView);
        favoriteButton.setImageResource(mFeedImagesData.getFavoriteButtonImage(position));
        imageView.setOnClickListener(mFeedImagesData.getImageClickListener(position));
        favoriteButton.setOnClickListener(mFeedImagesData.getFavoriteButtonClickListener(position));
    }

    @Override
    public int getItemCount()
    {
        return mFeedImagesData.getImagesCount();
    }
}
