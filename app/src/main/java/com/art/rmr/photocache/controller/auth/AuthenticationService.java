package com.art.rmr.photocache.controller.auth;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/**
 * Created by art090390 on 10.07.2015.
 */
public class AuthenticationService extends Service
{
    @Override
    public IBinder onBind(Intent intent)
    {
        PhotoCacheAuthenticator authenticator = new PhotoCacheAuthenticator(this);
        return authenticator.getIBinder();
    }
}
