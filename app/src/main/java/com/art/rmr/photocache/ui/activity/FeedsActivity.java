package com.art.rmr.photocache.ui.activity;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.util.Pair;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.art.rmr.photocache.R;
import com.art.rmr.photocache.controller.PaletteBroadcastReceiver;
import com.art.rmr.photocache.controller.auth.PhotoCacheAccountConstants;
import com.art.rmr.photocache.controller.interfaces.FeedActivityInterface;
import com.art.rmr.photocache.controller.operations.AccountOperation;
import com.art.rmr.photocache.controller.storage.FavoritesFileStorage;
import com.art.rmr.photocache.controller.storage.FavoritesStorage;
import com.art.rmr.photocache.ui.fragments.feeds.FavoriteFeedFragment;
import com.art.rmr.photocache.ui.fragments.feeds.InstagramFeedFragment;
import com.art.rmr.photocache.ui.fragments.viewing.ViewFragment;
import com.art.rmr.photocache.ui.views.ToolbarView;
import com.art.rmr.photocache.utils.ViewColorAnimator;
import com.redmadrobot.chronos.ChronosConnector;

import java.io.IOException;


public class FeedsActivity extends AppCompatActivity implements FeedActivityInterface
{
    private static final String OPERATION_TAG_ACCOUNT = "OPERATION_TAG_ACCOUNT";
    private static final String ARG_TOOLBAR_COLOR = "ARG_TOOLBAR_COLOR";
    public static final String RESULT_ARG_WAS_FAVORITE = "RESULT_ARG_WAS_FAVORITE";
    public static final String RESULT_ARG_META_DATE = "RESULT_ARG_META_DATE";
    private static final String META_POSITION = "META_POSITION";
    private static final String META_NAME = "META_NAME";
    private static final String META_URL = "META_URL";
    private static final String META_FEED = "META_FEED";
    public static final int FEED_FAVORITE = 0;
    public static final int FEED_INSTAGRAM = 1;

    private static final int RESULT_VIEW_IMAGE = 1;

    private ToolbarView mToolbar;
    private FavoritesStorage mFavoritesStorage;
    private Integer mActionBarColor;
    private BroadcastReceiver mPaletteReceiver;
    private ChronosConnector mConnector = new ChronosConnector();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mConnector.onCreate(this, savedInstanceState);
        setContentView(R.layout.activity_feeds);

        mToolbar = (ToolbarView)findViewById(R.id.toolbar_view);
        setSupportActionBar(mToolbar.getSupportActionBar());

        if (savedInstanceState != null && savedInstanceState.containsKey(ARG_TOOLBAR_COLOR))
        {
            mActionBarColor = savedInstanceState.getInt(ARG_TOOLBAR_COLOR);
            if (findViewById(R.id.container) != null)
            {
                mToolbar.setBackgroundColor(mActionBarColor);
            }
        }
        chooseAccount();
    }

    @Override
    protected void onNewIntent(Intent intent)
    {
        super.onNewIntent(intent);
        chooseAccount();
    }

    private void chooseAccount()
    {
        Pair<Boolean, Account> accountInfo = AccountOperation.getLastAccount(this);
        if (accountInfo.first)
        {
            onAccountChosen(accountInfo.second);
        }
        else
        {
            mConnector.runOperation(new AccountOperation(this), OPERATION_TAG_ACCOUNT, false);
        }
    }

    private void onOperationResultFailure(@NonNull Bundle bundle)
    {
        int code = bundle.getInt(AccountManager.KEY_ERROR_CODE);
        if (code != AccountManager.ERROR_CODE_CANCELED)
        {
            String errorMessage = bundle.getString(AccountManager.KEY_ERROR_MESSAGE);
            String message = getResources().getString(R.string.authentication_failed);
            if (errorMessage != null)
            {
                message += " - " + errorMessage;
            }
            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        }
        finish();
    }

    private void onOperationResultSuccess(@NonNull Bundle bundle)
    {
        String accountName = bundle.getString(AccountManager.KEY_ACCOUNT_NAME);
        if (accountName == null)
        {
            throw new IllegalStateException();
        }
        AccountOperation.setLastAccount(this, accountName);
        onAccountChosen(new Account(accountName, PhotoCacheAccountConstants.ACCOUNT_TYPE));
    }

    private void onOperationSuccess(@NonNull Bundle bundle)
    {
        boolean containsErrorCode = bundle.containsKey(AccountManager.KEY_ERROR_CODE);
        boolean containsErrorMessage = bundle.containsKey(AccountManager.KEY_ERROR_MESSAGE);
        if (containsErrorCode || containsErrorMessage)
        {
            onOperationResultFailure(bundle);
        }
        else
        {
            onOperationResultSuccess(bundle);
        }
    }

    private void onOperationFailure(String errorMessage)
    {
        String message = getResources().getString(R.string.authentication_failed);
        if (errorMessage != null)
        {
            message += " - " + errorMessage;
        }
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        finish();
    }

    @SuppressWarnings("unused")
    public void onOperationFinished(final AccountOperation.Result result)
    {
        if (result.isSuccessful())
        {
            onOperationSuccess(result.getOutput());
        }
        else
        {
            onOperationFailure(result.getErrorMessage());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_feeds, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if (id == R.id.action_logout)
        {
            AccountOperation.resetLastAccount(this);
            startActivity(new Intent(this, FeedsActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        mPaletteReceiver = new PaletteBroadcastReceiver(this)
        {
            @Override
            protected void onColorsReceived(int primary, int primaryDark, int accent)
            {
                int colorPrimary = getResources().getColor(R.color.color_primary);
                int oldColor = mActionBarColor == null ? colorPrimary : mActionBarColor;
                mActionBarColor = primary;
                new ViewColorAnimator(oldColor, mActionBarColor)
                {
                    @Override
                    protected void changeColor(int color)
                    {
                        mToolbar.setBackgroundColor(color);
                    }
                }.animate();
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(mPaletteReceiver, PaletteBroadcastReceiver.sPaletteFilter);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        mConnector.onResume();
    }

    @Override
    protected void onPause()
    {
        mConnector.onPause();
        super.onPause();
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mPaletteReceiver);
        mPaletteReceiver = null;
    }

    private void onAccountChosen(Account account)
    {
        mToolbar.setUser(account);
        mFavoritesStorage = new FavoritesFileStorage(this, account);
        ViewPager pager = (ViewPager)findViewById(R.id.view_pager);
        pager.setAdapter(new FeedsAdapter(getSupportFragmentManager(), account));
        mToolbar.getTabs().setupWithViewPager(pager);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        mConnector.onSaveInstanceState(outState);
        if (mActionBarColor != null)
        {
            outState.putInt(ARG_TOOLBAR_COLOR, mActionBarColor);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == RESULT_VIEW_IMAGE)
        {
            if (resultCode == RESULT_OK)
            {
                Bundle args = data.getExtras();
                Bundle meta = data.getBundleExtra(RESULT_ARG_META_DATE);
                boolean wasFavorite = args.getBoolean(RESULT_ARG_WAS_FAVORITE);
                if (wasFavorite)
                {
                    try
                    {
                        int position = meta.getInt(META_POSITION);
                        String name = meta.getString(META_NAME);
                        if (meta.getInt(META_FEED) == FEED_FAVORITE)
                        {
                            mFavoritesStorage.removeImageFromFavorites(position);
                        }
                        else
                        {
                            mFavoritesStorage.removeImageFromFavorites(name);
                        }
                    }
                    catch (IOException e)
                    {
                        Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    String name = meta.getString(META_NAME);
                    String url = meta.getString(META_URL);
                    new FavoritesStorage.SaveImageTask(this, getFavoritesStorage(), null, name, url).execute();
                }
            }
        }
        else
            super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public FavoritesStorage getFavoritesStorage()
    {
        return mFavoritesStorage;
    }

    private View.OnClickListener createImageClickListener(String imageUri, String imageName, int imagePosition, boolean isFavorite, int feed)
    {
        Bundle metaData = new Bundle();
        metaData.putString(META_URL, imageUri);
        metaData.putString(META_NAME, imageName);
        metaData.putInt(META_POSITION, imagePosition);
        metaData.putInt(META_FEED, feed);
        return new ImageClickListener(imageUri, isFavorite, metaData);
    }

    private View.OnClickListener createImageClickListenerLarge(final String imageUri)
    {
        return new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                FragmentManager fragmentManager = getSupportFragmentManager();
                Fragment fragment = fragmentManager.findFragmentById(R.id.container);
                if (fragment == null)
                {
                    ViewFragment viewFragment = ViewFragment.newInstance(imageUri);
                    fragmentManager.beginTransaction().add(R.id.container, viewFragment).commit();
                }
                else
                {
                    ViewFragment viewFragment = (ViewFragment)fragment;
                    viewFragment.updateImage(imageUri);
                }
            }
        };
    }

    @Override
    public View.OnClickListener getImageClickListener(String imageUri, String imageName, int imagePosition, boolean isFavorite, int feed)
    {
        View frame = findViewById(R.id.container);
        if (frame == null)
        {
            return createImageClickListener(imageUri, imageName, imagePosition, isFavorite, feed);
        }
        else
        {
            return createImageClickListenerLarge(imageUri);
        }
    }

    private class FeedsAdapter extends FragmentPagerAdapter
    {
        private Account mAccount;

        public FeedsAdapter(FragmentManager fm, Account account)
        {
            super(fm);
            mAccount = account;
        }

        @Override
        public Fragment getItem(int position)
        {
            switch (position)
            {
            case 0:
                return FavoriteFeedFragment.newInstance();
            case 1:
                return InstagramFeedFragment.newInstance(mAccount, true);
            default:
                return null;
            }
        }

        @Override
        public int getCount()
        {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position)
        {
            switch (position)
            {
            case 0:
                return FeedsActivity.this.getResources().getString(R.string.title_feed_favorite);
            case 1:
                return FeedsActivity.this.getResources().getString(R.string.title_feed_instagram);
            default:
                return "";
            }
        }
    }

    private class ImageClickListener implements View.OnClickListener
    {
        private String mImageUri;
        private boolean mIsFavorite;
        private Bundle mMetaData;

        public ImageClickListener(@NonNull String imageUri, boolean isFavorite, @NonNull Bundle metaData)
        {
            mImageUri = imageUri;
            mIsFavorite = isFavorite;
            mMetaData = metaData;
        }

        @Override
        public void onClick(View v)
        {
            Intent intent = new Intent(FeedsActivity.this, ImageActivity.class);
            intent.putExtra(ImageActivity.ARG_IMAGE_URI, mImageUri);
            intent.putExtra(ImageActivity.ARG_IS_FAVORITE, mIsFavorite);
            intent.putExtra(ImageActivity.ARG_META_DATA, mMetaData);
            startActivityForResult(intent, RESULT_VIEW_IMAGE);
        }
    }
}
