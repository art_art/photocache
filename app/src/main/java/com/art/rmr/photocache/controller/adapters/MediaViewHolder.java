package com.art.rmr.photocache.controller.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.art.rmr.photocache.R;

/**
 * Created by Артём on 25.07.2015.
 */
public class MediaViewHolder extends RecyclerView.ViewHolder
{
    private ImageView mImage;
    private ImageButton mFavoriteButton;

    public MediaViewHolder(View itemView)
    {
        super(itemView);

        mImage = (ImageView)itemView.findViewById(R.id.media_image);
        mFavoriteButton = (ImageButton)itemView.findViewById(R.id.media_favorite);
    }

    public ImageView getImage()
    {
        return mImage;
    }

    public ImageButton getFavoriteButton()
    {
        return mFavoriteButton;
    }
}
