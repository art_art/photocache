package com.art.rmr.photocache.controller.implementations.instagram;

import android.support.v7.widget.RecyclerView;

import com.art.rmr.photocache.controller.storage.FavoritesStorage;
import com.art.rmr.photocache.model.ui.DisplayMedia;

/**
 * Created by Артём on 27.07.2015.
 * Слушатель событий хранилища источника инстаграм.
 */
public class InstagramStorageEventsListener implements FavoritesStorage.EventsListener
{
    private InstagramFeedData mFeedData;
    private RecyclerView.Adapter mAdapter;

    public InstagramStorageEventsListener(InstagramFeedData feedData, RecyclerView.Adapter adapter)
    {
        mFeedData = feedData;
        mAdapter = adapter;
    }

    @Override
    public void onImageAddedToFavorites(String name, int position, String uri)
    {
        for (int i = 0; i < mFeedData.getImagesCount(); ++i)
        {
            DisplayMedia media = mFeedData.getImage(i);
            if (media.getName().compareTo(name) == 0)
            {
                if (!media.isFavorite())
                {
                    media.setFavorite(true);
                    media.setUri(uri);
                    mAdapter.notifyItemChanged(i);
                }
                break;
            }
        }
    }

    @Override
    public void onImageRemovedFromFavorites(int position, String name)
    {
        for (int i = 0; i < mFeedData.getImagesCount(); ++i)
        {
            DisplayMedia media = mFeedData.getImage(i);
            if (media.getName().compareTo(name) == 0)
            {
                if (media.isFavorite())
                {
                    media.setFavorite(false);
                    media.setUri(null);
                    mAdapter.notifyItemChanged(i);
                }
                break;
            }
        }
    }
}
