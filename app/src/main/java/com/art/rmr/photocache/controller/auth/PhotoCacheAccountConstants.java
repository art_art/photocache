package com.art.rmr.photocache.controller.auth;

/**
 * Created by art090390 on 10.07.2015.
 */
public class PhotoCacheAccountConstants
{
    public static final String ACCOUNT_TYPE = "com.art.rmr.photoCache";

    public static final String AUTHTOKEN_TYPE_BASE_ACCESS = "Base access";
    public static final String AUTHTOKEN_TYPE_BASE_ACCESS_LABEL = "Base access to an Instagram account";

    public static final String USER_DATA_ID = "USER_DATA_ID";
    public static final String USER_DATA_AVATAR = "USER_DATA_AVATAR";
}
