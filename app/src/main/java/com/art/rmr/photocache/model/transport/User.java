package com.art.rmr.photocache.model.transport;

import android.accounts.Account;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;

import com.art.rmr.photocache.controller.auth.GetAuthTokenException;
import com.art.rmr.photocache.controller.network.InstargramServerQueryRequest;
import com.art.rmr.photocache.controller.network.ServerRequest;
import com.art.rmr.photocache.utils.GsonFactory;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.security.cert.CertificateEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Артём on 12.07.2015.
 * Сущность пользователь.
 */
public class User extends Entity
{
    public static final String USER_ID_SELF = "self";
    private static final String FIELD_USER_NAME = "username";
    private static final String FIELD_FULL_NAME = "full_name";
    private static final String FIELD_PROFILE_PICTURE = "profile_picture";
    private static final String FIELD_BIO = "bio";
    private static final String FIELD_WEB_SITE = "website";
    private static final String FIELD_COUNTS = "counts";
    private static final String FIELD_COUNTS_MEDIA = "media";
    private static final String FIELD_COUNTS_FOLLOWS = "follows";
    private static final String FIELD_COUNTS_FOLLOWED_BY = "followed_by";
    private static final String PATH = "users";
    private static final String FEED = "feed";

    private String mUserName;
    private String mFullName;
    private String mPicture;
    private String mBio;
    private String mWebSite;
    private int mMedia;
    private int mFollows;
    private int mFollowedBy;

    private User(JsonObject userObject)
    {
        super(userObject);
        mUserName = userObject.get(FIELD_USER_NAME).getAsString();
        mFullName = userObject.get(FIELD_FULL_NAME).getAsString();
        mPicture = userObject.get(FIELD_PROFILE_PICTURE).getAsString();
        mBio = userObject.get(FIELD_BIO).getAsString();
        mWebSite = userObject.get(FIELD_WEB_SITE).getAsString();
        JsonObject counts = userObject.get(FIELD_COUNTS).getAsJsonObject();
        mMedia = counts.get(FIELD_COUNTS_MEDIA).getAsInt();
        mFollows = counts.get(FIELD_COUNTS_FOLLOWS).getAsInt();
        mFollowedBy = counts.get(FIELD_COUNTS_FOLLOWED_BY).getAsInt();
    }

    public String getUserName()
    {
        return mUserName;
    }

    public String getPicture()
    {
        return mPicture;
    }

    public static User query(Context context, String id, String token) throws IOException, ServerRequest.SslPinningException, CertificateEncodingException
    {
        QueryUserRequest request = new QueryUserRequest(context, id, token);
        request.execute(context);
        return request.getResult();
    }

    public static List<Media> querySelfFeed(Activity activity, Account account) throws GetAuthTokenException, AuthenticatorException, OperationCanceledException, IOException, ServerRequest.SslPinningException, CertificateEncodingException
    {
        QuerySelfFeedRequest request = new QuerySelfFeedRequest(activity, account);
        request.execute(activity);
        return request.getResult();
    }

    private static class QueryUserRequest extends InstargramServerQueryRequest<User>
    {
        public QueryUserRequest(Context context, String id, String token) throws MalformedURLException
        {
            super(new String[] {PATH, id, ""}, context, token);
        }

        @Override
        protected User parseResult(@NonNull JsonElement data)
        {
            return GsonFactory.createGson().fromJson(data, User.class);
        }
    }

    private static class QuerySelfFeedRequest extends InstargramServerQueryRequest<List<Media>>
    {
        public QuerySelfFeedRequest(@NonNull Activity activity, @NonNull Account account) throws GetAuthTokenException, AuthenticatorException, OperationCanceledException, IOException
        {
            super(new String[] {PATH, USER_ID_SELF, FEED}, activity, account);
        }

        @Override
        protected List<Media> parseResult(@NonNull JsonElement data)
        {
            List<Media> result = new ArrayList<>();
            JsonArray medias = data.getAsJsonArray();
            Gson json = GsonFactory.createGson();
            for (JsonElement elem : medias)
                result.add(json.fromJson(elem, Media.class));
            return result;
        }
    }

    public static class UserDeserializer implements JsonDeserializer<User>
    {
        @Override
        public User deserialize(@NonNull JsonElement json, @NonNull Type typeOfT, @NonNull JsonDeserializationContext context) throws JsonParseException
        {
            return new User(json.getAsJsonObject());
        }
    }
}
