package com.art.rmr.photocache.controller.network;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.art.rmr.photocache.controller.auth.GetAuthTokenException;
import com.art.rmr.photocache.controller.auth.PhotoCacheAccountConstants;
import com.art.rmr.photocache.utils.GsonFactory;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.MalformedURLException;

/**
 * Created by art090390 on 13.07.2015.
 * Запрос на сервер Instagram.
 */
public class InstagramServerRequest extends AuthenticatedServerRequest
{
    private static final String PROTOCOL = "https";
    private static final String HOST = "api.instagram.com/v1";

    private Context mContext;

    public InstagramServerRequest(@NonNull String[] pathSegments, boolean hasOutput, boolean hasInput, @NonNull Context context, @NonNull String token) throws MalformedURLException
    {
        super(getUrl(pathSegments), hasOutput, hasInput, token);
        mContext = context;
    }

    public InstagramServerRequest(@NonNull String[] pathSegments, boolean hasOutput, boolean hasInput, @NonNull Activity activity, @NonNull Account account) throws GetAuthTokenException, AuthenticatorException, OperationCanceledException, IOException
    {
        super(getUrl(pathSegments), hasOutput, hasInput, getToken(activity, account));
        mContext = activity;
    }

    @NonNull
    private static Uri.Builder getUrl(@NonNull String[] pathSegments)
    {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme(PROTOCOL);
        builder.encodedAuthority(HOST);
        for (String segment : pathSegments)
            builder.appendPath(segment);
        return builder;
    }

    @NonNull
    private static String getToken(@NonNull Activity activity, @NonNull Account account) throws AuthenticatorException, OperationCanceledException, IOException, GetAuthTokenException
    {
        AccountManager manager = AccountManager.get(activity);
        AccountManagerFuture<Bundle> future = manager.getAuthToken(account, PhotoCacheAccountConstants.AUTHTOKEN_TYPE_BASE_ACCESS, null, activity, null, null);
        Bundle result = future.getResult();
        if (!result.containsKey(AccountManager.KEY_AUTHTOKEN))
        {
            throw new GetAuthTokenException(result.getString(AccountManager.KEY_ERROR_MESSAGE));
        }
        return result.getString(AccountManager.KEY_AUTHTOKEN);
    }

    protected void input(@NonNull JsonElement data)
    {
    }

    @Override
    protected final void input(@NonNull InputStream inputStream) throws IOException
    {
        String json = readStream(inputStream);
        InstagramJson ij = GsonFactory.createGson().fromJson(json, InstagramJson.class);
        JsonObject meta = ij.getMeta();
        if (meta.get(InstagramJson.FIELD_META_CODE).getAsInt() != InstagramJson.CODE_SUCCESS)
        {
            String errorType = meta.get(InstagramJson.FIELD_META_ERROR_TYPE).getAsString();
            if (errorType.compareTo("OAuthAccessTokenException") == 0)
            {
                AccountManager manager = AccountManager.get(mContext);
                manager.invalidateAuthToken(PhotoCacheAccountConstants.ACCOUNT_TYPE, getToken());
            }

            String error = meta.get(InstagramJson.FIELD_META_ERROR_MESSAGE).getAsString();
            throw new IOException(error);
        }
        input(ij.getData());
    }

    public static class InstagramJson
    {
        public static final String FIELD_META = "meta";
        public static final String FIELD_META_CODE = "code";
        public static final String FIELD_META_ERROR_TYPE = "error_type";
        public static final String FIELD_META_ERROR_MESSAGE = "error_message";
        public static final String FIELD_DATA = "data";
        public static final int CODE_SUCCESS = 200;
        private JsonElement mData;
        private JsonObject mMeta;

        private InstagramJson()
        {
        }

        public JsonElement getData()
        {
            return mData;
        }

        @NonNull
        public JsonObject getMeta()
        {
            return mMeta;
        }

        public static class IJDeserializer implements JsonDeserializer<InstagramJson>
        {
            @Override
            public InstagramJson deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException
            {
                JsonObject object = json.getAsJsonObject();
                InstagramJson result = new InstagramJson();
                result.mMeta = object.getAsJsonObject(FIELD_META);
                if (result.mMeta.get(FIELD_META_CODE).getAsInt() == CODE_SUCCESS)
                {
                    result.mData = object.get(FIELD_DATA);
                }
                return result;
            }
        }
    }
}
