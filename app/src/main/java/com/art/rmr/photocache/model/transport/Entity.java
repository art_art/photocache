package com.art.rmr.photocache.model.transport;

import com.google.gson.JsonObject;

/**
 * Created by Артём on 12.07.2015.
 * Базовый класс всез моделей
 */
public abstract class Entity
{
    private static final String FIELD_ID = "id";
    protected static final String FIELD_DATA = "data";

    private String mId;

    protected Entity(JsonObject entityObject)
    {
        mId = entityObject.get(FIELD_ID).getAsString();
    }

    public String getId()
    {
        return mId;
    }
}
