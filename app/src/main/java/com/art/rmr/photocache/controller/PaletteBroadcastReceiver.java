package com.art.rmr.photocache.controller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;

import com.art.rmr.photocache.R;

/**
 * Created by art090390 on 29.07.2015.
 * Ресивер цветов палитры.
 */
public abstract class PaletteBroadcastReceiver extends BroadcastReceiver
{
    public static final String ACTION_PALETTE_GENERATED = "ACTION_PALETTE_GENERATED";
    public static final String RESULT_PRIMARY = "RESULT_PRIMARY";
    public static final String RESULT_PRIMARY_DARK = "RESULT_PRIMARY_DARK";
    public static final String RESULT_ACCENT = "RESULT_ACCENT";

    public static final IntentFilter sPaletteFilter = new IntentFilter(ACTION_PALETTE_GENERATED);

    private int mDefaultPrimary;
    private int mDefaultPrimaryDark;
    private int mDefaultAccent;

    protected PaletteBroadcastReceiver(Context context)
    {
        Resources resources = context.getResources();
        mDefaultAccent = resources.getColor(R.color.color_accent);
        mDefaultPrimary = resources.getColor(R.color.color_primary);
        mDefaultPrimaryDark = resources.getColor(R.color.color_primary_dark);
    }

    protected abstract void onColorsReceived(int primary, int primaryDark, int accent);

    @Override
    public void onReceive(Context context, Intent intent)
    {
        int primary = intent.getIntExtra(RESULT_PRIMARY, mDefaultPrimary);
        int primaryDark = intent.getIntExtra(RESULT_PRIMARY_DARK, mDefaultPrimaryDark);
        int accent = intent.getIntExtra(RESULT_ACCENT, mDefaultAccent);
        onColorsReceived(primary, primaryDark, accent);
    }
}
