package com.art.rmr.photocache.ui.fragments.feeds;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.art.rmr.photocache.R;
import com.art.rmr.photocache.controller.adapters.MediaAdapter;
import com.art.rmr.photocache.controller.click.RemoveByPositionClickListener;
import com.art.rmr.photocache.controller.implementations.favorites.FavoritesFeedData;
import com.art.rmr.photocache.controller.interfaces.FeedImagesData;
import com.art.rmr.photocache.controller.storage.FavoritesStorage;
import com.art.rmr.photocache.ui.activity.FeedsActivity;
import com.art.rmr.photocache.ui.views.RecyclerViewWithEmpty;

public class FavoriteFeedFragment extends BaseFeedFragment implements FeedImagesData, FavoritesStorage.EventsListener
{
    private FavoritesFeedData mFeedData;
    private MediaAdapter mAdapter;

    public FavoriteFeedFragment()
    {
    }

    public static FavoriteFeedFragment newInstance()
    {
        FavoriteFeedFragment fragment = new FavoriteFeedFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        mFeedData = new FavoritesFeedData(getActivityInterface());
        mAdapter = new MediaAdapter(getActivity(), this);
    }

    @Override
    protected FavoritesStorage.EventsListener getFavoriteStorageEventsListener()
    {
        return this;
    }

    @Override
    protected void notifyDataSetChanged()
    {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_favorite_feed, container, false);

        RecyclerViewWithEmpty feed = (RecyclerViewWithEmpty)rootView.findViewById(R.id.feed);
        feed.setEmptyView(rootView.findViewById(R.id.empty));
        feed.setLayoutManager(new LinearLayoutManager(getActivity()));
        feed.setAdapter(mAdapter);

        return rootView;
    }

    @Override
    public int getImagesCount()
    {
        return mFeedData.getImagesCount();
    }

    @Override
    public int getFavoriteButtonImage(int position)
    {
        return R.mipmap.ic_star_filled;
    }

    @Override
    public String getImageUri(int position)
    {
        return mFeedData.getFavoriteData(position).getUri();
    }

    @Override
    public View.OnClickListener getFavoriteButtonClickListener(int position)
    {
        return new RemoveByPositionClickListener(getActivity(), position, getActivityInterface().getFavoritesStorage());
    }

    @Override
    public View.OnClickListener getImageClickListener(int position)
    {
        FavoritesFeedData.FavoriteImageData imageData = mFeedData.getFavoriteData(position);
        String uri = imageData.getUri();
        String name = imageData.getName();
        return getActivityInterface().getImageClickListener(uri, name, position, true, FeedsActivity.FEED_FAVORITE);
    }

    @Override
    public void onImageAddedToFavorites(String name, int position, String uri)
    {
        mAdapter.notifyItemInserted(position);
    }

    @Override
    public void onImageRemovedFromFavorites(int position, String name)
    {
        mAdapter.notifyItemRemoved(position);
    }
}
