package com.art.rmr.photocache.ui.fragments.feeds;

import android.os.Bundle;

import com.art.rmr.photocache.controller.interfaces.FeedActivityInterface;
import com.art.rmr.photocache.controller.storage.FavoritesStorage;
import com.art.rmr.photocache.ui.fragments.BaseFragment;

/**
 * Created by art090390 on 28.07.2015.
 */
public abstract class BaseFeedFragment extends BaseFragment<FeedActivityInterface>
{
    private long mStorageChangeTimeStamp;

    protected BaseFeedFragment()
    {
    }

    protected abstract FavoritesStorage.EventsListener getFavoriteStorageEventsListener();

    protected abstract void notifyDataSetChanged();

    @Override
    public void onStart()
    {
        super.onStart();
        FavoritesStorage storage = getActivityInterface().getFavoritesStorage();
        storage.subscribeEvents(getFavoriteStorageEventsListener());
        if (storage.getChangeTimeStamp() > mStorageChangeTimeStamp)
            notifyDataSetChanged();
    }

    @Override
    public void onStop()
    {
        super.onStop();
        FavoritesStorage storage = getActivityInterface().getFavoritesStorage();
        mStorageChangeTimeStamp = storage.getChangeTimeStamp();
        storage.unsubscribeEvents(getFavoriteStorageEventsListener());
    }
}
