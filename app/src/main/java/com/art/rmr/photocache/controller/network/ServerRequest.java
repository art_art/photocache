package com.art.rmr.photocache.controller.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;

import com.art.rmr.photocache.R;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLPeerUnverifiedException;

/**
 * Created by Артём on 12.07.2015.
 * Запрос на сервер.
 */
public class ServerRequest
{
    private static boolean sServerCertificateValidated = false;
    private URL mUrl;
    private boolean mHasOutput;
    private boolean mHasInput;

    protected ServerRequest(@NonNull String url, boolean hasOutput, boolean hasInput) throws MalformedURLException
    {
        mUrl = new URL(url);
        mHasInput = hasInput;
        mHasOutput = hasOutput;
    }

    public static boolean isNetworkAvailable(@NonNull Context context)
    {
        ConnectivityManager connectivityManager =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void isNetworkAvailableThrow(@NonNull Context context) throws IOException
    {
        if (!isNetworkAvailable(context))
        {
            throw new IOException(context.getString(R.string.message_network_unavailable));
        }
    }

    public static boolean compare(@NonNull byte[] first, @NonNull byte[] second)
    {
        if (first.length != second.length)
        {
            return false;
        }

        for (int i = 0; i < first.length; ++i)
            if (first[i] != second[i])
            {
                return false;
            }

        return true;
    }

    @NonNull
    public static String readStream(@NonNull InputStream in) throws IOException
    {
        BufferedReader r = new BufferedReader(new InputStreamReader(in));
        StringBuilder total = new StringBuilder();
        String line;
        while ((line = r.readLine()) != null)
            total.append(line);
        return total.toString();
    }

    @SuppressWarnings("unused")
    public static void writeStream(@NonNull String data, @NonNull OutputStream out) throws IOException
    {
        DataOutputStream dos = new DataOutputStream(out);
        dos.write(data.getBytes());
        dos.flush();
    }

    @NonNull
    private static byte[] readBytes(@NonNull InputStream inputStream) throws IOException
    {
        // this dynamically extends to take the bytes you read
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();

        // this is storage overwritten on each iteration with bytes
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        // we need to know how may bytes were read to write them to the byteBuffer
        int len;
        while ((len = inputStream.read(buffer)) != -1)
        {
            byteBuffer.write(buffer, 0, len);
        }

        // and then we can return your byte array.
        return byteBuffer.toByteArray();
    }

    @NonNull
    private static byte[] readCertificate(@NonNull Context context) throws IOException
    {
        return readBytes(context.getAssets().open("server.cer"));
    }

    private static synchronized void validateServerCertificate(@NonNull Context context, @NonNull HttpsURLConnection urlConnection) throws SslPinningException, CertificateEncodingException, IOException
    {
        if (!sServerCertificateValidated)
        {
            byte[] serverCertificate = readCertificate(context);
            Certificate[] certs = urlConnection.getServerCertificates();
            for (Certificate cert : certs)
                if (compare(serverCertificate, cert.getEncoded()))
                {
                    sServerCertificateValidated = true;
                    return;
                }
            throw new SslPinningException();
        }
    }

    @NonNull
    protected final HttpURLConnection createConnection(@NonNull Context context, boolean useSslPinning) throws IOException, SslPinningException, CertificateEncodingException
    {
        HttpsURLConnection urlConnection = (HttpsURLConnection)mUrl.openConnection();
        if (useSslPinning)
        {
            try
            {
                urlConnection.connect();
                validateServerCertificate(context, urlConnection);
            }
            catch (SslPinningException | CertificateEncodingException e)
            {
                urlConnection.disconnect();
                throw e;
            }
        }
        return urlConnection;
    }

    protected void prepareOutput(@NonNull HttpURLConnection urlConnection) throws ProtocolException
    {
        urlConnection.setDoOutput(true);
        urlConnection.setRequestMethod("POST");
    }

    protected void output(@NonNull OutputStream outputStream) throws IOException
    {
    }

    protected void input(@NonNull InputStream inputStream) throws IOException
    {
    }

    private void writeOutput(@NonNull HttpURLConnection connection) throws IOException
    {
        prepareOutput(connection);
        OutputStream outRaw = connection.getOutputStream();
        try
        {
            output(outRaw);
        }
        finally
        {
            outRaw.close();
        }
    }

    private void readInput(@NonNull HttpURLConnection connection) throws IOException
    {
        InputStream inRaw = connection.getInputStream();
        try
        {
            InputStream in = new BufferedInputStream(inRaw);
            input(in);
        }
        finally
        {
            inRaw.close();
        }
    }

    public void execute(@NonNull Context context, boolean useSslPinning) throws IOException, SslPinningException, CertificateEncodingException
    {
        isNetworkAvailableThrow(context);

        HttpURLConnection urlConnection = createConnection(context, useSslPinning);
        try
        {
            if (mHasOutput)
            {
                writeOutput(urlConnection);
            }

            int response = urlConnection.getResponseCode();
            if (response != HttpURLConnection.HTTP_OK)
            {
                throw new IOException(urlConnection.getResponseMessage());
            }

            if (mHasInput)
            {
                readInput(urlConnection);
            }
        }
        finally
        {
            urlConnection.disconnect();
        }
    }

    public void execute(@NonNull Context context) throws SslPinningException, CertificateEncodingException, IOException
    {
        execute(context, true);
    }

    public static class SslPinningException extends Exception
    {
        public SslPinningException()
        {
            super("Server certificate differs");
        }
    }
}
