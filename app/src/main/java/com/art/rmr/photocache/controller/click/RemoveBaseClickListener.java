package com.art.rmr.photocache.controller.click;

import android.content.Context;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;

/**
 * Created by Артём on 26.07.2015.
 * Базовый слушатель для снятия отметки избранное.
 */
public abstract class RemoveBaseClickListener implements View.OnClickListener
{
    private Context mContext;

    protected RemoveBaseClickListener(Context context)
    {
        mContext = context;
    }

    protected abstract void remove() throws IOException;

    @Override
    public void onClick(View v)
    {
        try
        {
            remove();
        }
        catch (IOException e)
        {
            Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
