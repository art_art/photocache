package com.art.rmr.photocache.ui.activity;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import com.art.rmr.photocache.R;
import com.art.rmr.photocache.controller.PaletteBroadcastReceiver;
import com.art.rmr.photocache.controller.interfaces.MarkActivityInterface;
import com.art.rmr.photocache.ui.fragments.viewing.ViewMarkFragment;
import com.art.rmr.photocache.utils.ViewColorAnimator;

public class ImageActivity extends AppCompatActivity implements MarkActivityInterface
{
    public static final String ARG_IMAGE_URI = "ARG_IMAGE_URI";
    public static final String ARG_IS_FAVORITE = "ARG_IS_FAVORITE";
    public static final String ARG_META_DATA = "ARG_META_DATA";
    private static final String ARG_ACTION_BAR_COLOR = "ARG_ACTION_BAR_COLOR";

    private Bundle mMetaData;
    private Integer mActionBarColor;
    private BroadcastReceiver mPaletteReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

        if (savedInstanceState == null)
        {
            Bundle extras = getIntent().getExtras();
            mMetaData = extras.getBundle(ARG_META_DATA);
            String imageUri = extras.getString(ARG_IMAGE_URI);
            boolean isFavorite = extras.getBoolean(ARG_IS_FAVORITE);
            Fragment fragment = ViewMarkFragment.newInstance(imageUri, isFavorite);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.container, fragment).commit();
        }
        else
        {
            mMetaData = savedInstanceState.getBundle(ARG_META_DATA);
            if (savedInstanceState.containsKey(ARG_ACTION_BAR_COLOR))
            {
                mActionBarColor = savedInstanceState.getInt(ARG_ACTION_BAR_COLOR);
                getSupportActionBar().setBackgroundDrawable(new ColorDrawable(mActionBarColor));
            }
        }
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        mPaletteReceiver = new PaletteBroadcastReceiver(this)
        {
            @Override
            protected void onColorsReceived(int primary, int primaryDark, int accent)
            {
                int colorPrimary = getResources().getColor(R.color.color_primary);
                int oldColor = mActionBarColor == null ? colorPrimary : mActionBarColor;
                mActionBarColor = primary;
                final ActionBar actionBar = getSupportActionBar();
                new ViewColorAnimator(oldColor, mActionBarColor)
                {
                    @Override
                    protected void changeColor(int color)
                    {
                        actionBar.setBackgroundDrawable(new ColorDrawable(color));
                    }
                }.animate();
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(mPaletteReceiver, PaletteBroadcastReceiver.sPaletteFilter);
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mPaletteReceiver);
        mPaletteReceiver = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putBundle(ARG_META_DATA, mMetaData);
        if (mActionBarColor != null)
        {
            outState.putInt(ARG_ACTION_BAR_COLOR, mActionBarColor);
        }
    }

    @Override
    public void marked(boolean wasFavorite)
    {
        Intent resultIntent = new Intent();
        resultIntent.putExtra(FeedsActivity.RESULT_ARG_WAS_FAVORITE, wasFavorite);
        resultIntent.putExtra(FeedsActivity.RESULT_ARG_META_DATE, mMetaData);
        setResult(RESULT_OK, resultIntent);
        finish();
    }
}
