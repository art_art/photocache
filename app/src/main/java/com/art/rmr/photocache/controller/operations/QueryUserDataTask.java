package com.art.rmr.photocache.controller.operations;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.art.rmr.photocache.model.transport.User;


/**
 * Created by art090390 on 20.07.2015.
 * Операция чтения данных пользователя.
 */
public abstract class QueryUserDataTask extends AsyncTask<String, Void, Bundle>
{
    private static final String RESULT_USERNAME = "RESULT_USERNAME";
    private static final String RESULT_USERID = "RESULT_USERID";
    private static final String RESULT_TOKEN = "RESULT_TOKEN";
    private static final String RESULT_AVATAR = "RESULT_AVATAR";

    private Context mContext;

    public QueryUserDataTask(@NonNull Context context)
    {
        mContext = context;
    }

    @Override
    protected Bundle doInBackground(@NonNull String... params)
    {
        Bundle result = null;
        try
        {
            String token = params[0];
            User user = User.query(mContext, User.USER_ID_SELF, token);
            result = new Bundle();
            result.putString(RESULT_USERNAME, user.getUserName());
            result.putString(RESULT_USERID, user.getId());
            result.putString(RESULT_TOKEN, token);
            result.putString(RESULT_AVATAR, user.getPicture());
        }
        catch (Exception ignored)
        {
        }
        return result;
    }

    protected abstract void onError();

    protected abstract void onSuccess(@NonNull String userId, @NonNull String userName, @NonNull String token, String avatar);

    @Override
    protected void onPostExecute(Bundle result)
    {
        super.onPostExecute(result);

        if (result == null)
        {
            onError();
        }
        else
        {
            boolean containsName = result.containsKey(RESULT_USERNAME);
            boolean containsId = result.containsKey(RESULT_USERID);
            boolean containsToken = result.containsKey(RESULT_TOKEN);
            if (!(containsName && containsId && containsToken))
            {
                throw new IllegalStateException();
            }
            String name = result.getString(RESULT_USERNAME);
            String id = result.getString(RESULT_USERID);
            String token = result.getString(RESULT_TOKEN);
            String avatar = result.getString(RESULT_AVATAR);
            onSuccess(id, name, token, avatar);
        }
    }
}
