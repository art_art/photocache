package com.art.rmr.photocache.controller.implementations.favorites;

import com.art.rmr.photocache.controller.interfaces.FeedActivityInterface;
import com.art.rmr.photocache.controller.storage.FavoritesStorage;

/**
 * Created by Артём on 26.07.2015.
 * Класс данных избранных фото.
 */
public class FavoritesFeedData
{
    private FeedActivityInterface mActivityInterface;

    public FavoritesFeedData(FeedActivityInterface activityInterface)
    {
        mActivityInterface = activityInterface;
    }

    public int getImagesCount()
    {
        return mActivityInterface.getFavoritesStorage().getFavoritesCount();
    }

    public FavoriteImageData getFavoriteData(int position)
    {
        FavoritesStorage.FavoriteInfo favoriteInfo = mActivityInterface.getFavoritesStorage().getFavoriteImage(position);
        return new FavoriteImageData(favoriteInfo.getName(), favoriteInfo.getUri());
    }

    public static class FavoriteImageData
    {
        private String mName;
        private String mUri;

        public FavoriteImageData(String name, String uri)
        {
            mName = name;
            mUri = uri;
        }

        public String getName()
        {
            return mName;
        }

        public String getUri()
        {
            return mUri;
        }
    }
}
