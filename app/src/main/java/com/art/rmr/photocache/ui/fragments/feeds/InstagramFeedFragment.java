package com.art.rmr.photocache.ui.fragments.feeds;

import android.accounts.Account;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.art.rmr.photocache.R;
import com.art.rmr.photocache.controller.adapters.MediaAdapter;
import com.art.rmr.photocache.controller.adapters.MediaViewHolder;
import com.art.rmr.photocache.controller.click.AddToFavoriteClickListener;
import com.art.rmr.photocache.controller.click.RemoveByNameClickListener;
import com.art.rmr.photocache.controller.implementations.instagram.InstagramFeedData;
import com.art.rmr.photocache.controller.implementations.instagram.InstagramFeedDataSaver;
import com.art.rmr.photocache.controller.implementations.instagram.InstagramImagesAccumulator;
import com.art.rmr.photocache.controller.implementations.instagram.InstagramStorageEventsListener;
import com.art.rmr.photocache.controller.interfaces.FeedImagesData;
import com.art.rmr.photocache.controller.interfaces.ImagesAccumulator;
import com.art.rmr.photocache.controller.operations.RefreshTask;
import com.art.rmr.photocache.controller.storage.FavoritesStorage;
import com.art.rmr.photocache.model.ui.DisplayMedia;
import com.art.rmr.photocache.ui.activity.FeedsActivity;
import com.art.rmr.photocache.ui.views.RecyclerViewWithEmpty;

public class InstagramFeedFragment extends BaseFeedFragment implements SwipeRefreshLayout.OnRefreshListener, FeedImagesData
{
    public static final String ARG_ACCOUNT = "ARG_ACCOUNT";
    public static final String ARG_TO_REFRESH = "ARG_TO_REFRESH";

    private MediaAdapter mAdapter;
    private InstagramFeedData mFeedData;
    private SwipeRefreshLayout mRefreshLayout;
    private InstagramStorageEventsListener mEventsListener;
    private RefreshTask mRefreshTask;
    private Account mAccount;
    private RecyclerView mImagesList;

    public InstagramFeedFragment()
    {
    }

    public static InstagramFeedFragment newInstance(Account account, boolean toRefresh)
    {
        InstagramFeedFragment fragment = new InstagramFeedFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_ACCOUNT, account);
        args.putBoolean(ARG_TO_REFRESH, toRefresh);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        mFeedData = new InstagramFeedData();
        mAdapter = new MediaAdapter(getActivity(), this);
        mEventsListener = new InstagramStorageEventsListener(mFeedData, mAdapter);

        if (savedInstanceState != null)
        {
            InstagramFeedDataSaver feedDataSaver = new InstagramFeedDataSaver(mFeedData);
            feedDataSaver.load(savedInstanceState);
            mAccount = savedInstanceState.getParcelable(ARG_ACCOUNT);
        }
        else
        {
            mAccount = getArguments().getParcelable(ARG_ACCOUNT);
        }
    }

    @Override
    protected FavoritesStorage.EventsListener getFavoriteStorageEventsListener()
    {
        return mEventsListener;
    }

    @Override
    protected void notifyDataSetChanged()
    {
        mFeedData.refreshState(getActivityInterface().getFavoritesStorage(), mAdapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_net_feed, container, false);

        mRefreshLayout = (SwipeRefreshLayout)rootView.findViewById(R.id.refresh);
        mRefreshLayout.setOnRefreshListener(this);
        mRefreshLayout.setColorSchemeResources(R.color.instagram, R.color.accent, R.color.instagram_dark);

        RecyclerViewWithEmpty feed = (RecyclerViewWithEmpty)rootView.findViewById(R.id.feed);
        feed.setLayoutManager(new LinearLayoutManager(getActivity()));
        feed.setAdapter(mAdapter);
        feed.setEmptyView(rootView.findViewById(R.id.empty));
        mImagesList = feed;

        if (savedInstanceState == null)
        {
            if (getArguments().getBoolean(ARG_TO_REFRESH))
            {
                onRefresh();
            }
        }

        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putParcelable(ARG_ACCOUNT, mAccount);
        InstagramFeedDataSaver feedDataSaver = new InstagramFeedDataSaver(mFeedData);
        feedDataSaver.save(outState);
    }

    @Override
    public void onStop()
    {
        super.onStop();
        synchronized (InstagramFeedFragment.this)
        {
            if (mRefreshTask != null)
            {
                mRefreshTask.cancel(true);
                mRefreshTask = null;
            }
        }
    }

    @Override
    public void onRefresh()
    {
        ImagesAccumulator accumulator = new InstagramImagesAccumulator(mAdapter, mFeedData, getActivityInterface());
        new RefreshTask(getActivity(), mAccount, accumulator)
        {
            @Override
            protected void onTaskStarted()
            {
                mRefreshLayout.setRefreshing(true);
                synchronized (InstagramFeedFragment.this)
                {
                    mRefreshTask = this;
                }
            }

            @Override
            protected void onTaskFinished()
            {
                mRefreshLayout.setRefreshing(false);
                synchronized (InstagramFeedFragment.this)
                {
                    mRefreshTask = null;
                }
            }
        }.execute();
    }

    @Override
    public int getImagesCount()
    {
        return mFeedData.getImagesCount();
    }

    @Override
    public int getFavoriteButtonImage(int position)
    {
        DisplayMedia media = mFeedData.getImage(position);
        return media.isFavorite() ? R.mipmap.ic_star_filled : R.mipmap.ic_star_outlined;
    }

    @Override
    public String getImageUri(int position)
    {
        DisplayMedia media = mFeedData.getImage(position);
        return media.isFavorite() ? media.getUri() : media.getUrl();
    }

    @Override
    public View.OnClickListener getFavoriteButtonClickListener(int position)
    {
        DisplayMedia media = mFeedData.getImage(position);
        Context context = getActivity();
        FavoritesStorage favoritesStorage = getActivityInterface().getFavoritesStorage();
        String imageName = media.getName();
        if (media.isFavorite())
        {
            return new RemoveByNameClickListener(context, favoritesStorage, imageName);
        }
        else
        {
            return new AddToFavoriteClickListener(context, favoritesStorage, imageName, media.getUrl());
        }
    }

    @Override
    public View.OnClickListener getImageClickListener(int position)
    {
        DisplayMedia media = mFeedData.getImage(position);
        String name = media.getName();
        boolean isFavorite = media.isFavorite();
        return getActivityInterface().getImageClickListener(getImageUri(position), name, position, isFavorite, FeedsActivity.FEED_INSTAGRAM);
    }
}
