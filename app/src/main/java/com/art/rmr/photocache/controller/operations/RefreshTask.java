package com.art.rmr.photocache.controller.operations;

import android.accounts.Account;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Activity;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.art.rmr.photocache.R;
import com.art.rmr.photocache.controller.auth.GetAuthTokenException;
import com.art.rmr.photocache.controller.interfaces.ImagesAccumulator;
import com.art.rmr.photocache.controller.network.ServerRequest;
import com.art.rmr.photocache.model.transport.Media;
import com.art.rmr.photocache.model.transport.User;

import java.io.IOException;
import java.security.cert.CertificateEncodingException;
import java.util.List;

/**
 * Created by Артём on 25.07.2015.
 * Операция обновления фото из сети.
 */
public abstract class RefreshTask extends AsyncTask<Void, Void, RefreshTask.RefreshResult>
{
    private Activity mActivity;
    private Account mAccount;
    private ImagesAccumulator mImagesAccumulator;

    public RefreshTask(Activity activity, Account account, ImagesAccumulator imagesAccumulator)
    {
        mActivity = activity;
        mAccount = account;
        mImagesAccumulator = imagesAccumulator;
    }

    @Override
    protected RefreshResult doInBackground(Void... params)
    {
        RefreshResult result;
        try
        {
            List<Media> medias = User.querySelfFeed(mActivity, mAccount);
            int count = mImagesAccumulator.addNewImages(medias);
            result = RefreshResult.newResultSuccess(count);
        }
        catch (GetAuthTokenException | AuthenticatorException e)
        {
            result = RefreshResult.newResultError(R.string.message_relogin);
        }
        catch (OperationCanceledException e)
        {
            result = new RefreshResult();
        }
        catch (IOException e)
        {
            result = RefreshResult.newResultError(R.string.message_network_error);
        }
        catch (ServerRequest.SslPinningException | CertificateEncodingException e)
        {
            result = RefreshResult.newResultError(R.string.message_update);
        }
        return result;
    }

    protected abstract void onTaskStarted();

    @Override
    protected void onPreExecute()
    {
        super.onPreExecute();
        onTaskStarted();
    }

    protected abstract void onTaskFinished();

    @Override
    protected void onPostExecute(@NonNull RefreshResult result)
    {
        super.onPostExecute(result);

        if (result.getResult())
        {
            Integer imagesCount = result.getAddedImagesCount();
            mImagesAccumulator.onImagesAdded(imagesCount == null ? 0 : imagesCount);
        }
        else
        {
            String errorMessage = mActivity.getResources().getString(R.string.message_refresh_failed);
            Integer messageCode = result.getErrorMessage();
            if (messageCode != null)
            {
                errorMessage += " - " + mActivity.getResources().getString(messageCode);
            }
            Toast.makeText(mActivity, errorMessage, Toast.LENGTH_SHORT).show();
        }
        onTaskFinished();
    }

    @Override
    protected void onCancelled(RefreshResult result)
    {
        super.onCancelled(result);
        onTaskFinished();
    }

    protected static class RefreshResult
    {
        private boolean mResult;
        private Integer mAddedImagesCount;
        private Integer mErrorMessage;

        public RefreshResult()
        {
            mResult = false;
        }

        public static RefreshResult newResultError(int errorMessage)
        {
            RefreshResult result = new RefreshResult();
            result.mErrorMessage = errorMessage;
            return result;
        }

        public static RefreshResult newResultSuccess(int addedCount)
        {
            RefreshResult result = new RefreshResult();
            result.mResult = true;
            result.mAddedImagesCount = addedCount;
            return result;
        }

        public boolean getResult()
        {
            return mResult;
        }

        @Nullable
        public Integer getAddedImagesCount()
        {
            return mAddedImagesCount;
        }

        @Nullable
        public Integer getErrorMessage()
        {
            return mErrorMessage;
        }
    }
}
