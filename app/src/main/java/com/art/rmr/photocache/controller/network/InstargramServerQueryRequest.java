package com.art.rmr.photocache.controller.network;

import android.accounts.Account;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;

import com.art.rmr.photocache.controller.auth.GetAuthTokenException;
import com.google.gson.JsonElement;

import java.io.IOException;
import java.net.MalformedURLException;

/**
 * Created by art090390 on 14.07.2015.
 * Запрос на чтение на сервер Instagram.
 */
public abstract class InstargramServerQueryRequest<T> extends InstagramServerRequest
{
    private T mResult;

    protected InstargramServerQueryRequest(@NonNull String[] pathSegments, @NonNull Context context, @NonNull String token) throws MalformedURLException
    {
        super(pathSegments, false, true, context, token);
    }

    protected InstargramServerQueryRequest(@NonNull String[] pathSegments, @NonNull Activity activity, @NonNull Account account) throws GetAuthTokenException, AuthenticatorException, OperationCanceledException, IOException
    {
        super(pathSegments, false, true, activity, account);
    }

    @NonNull
    protected abstract T parseResult(@NonNull JsonElement data);

    @Override
    @NonNull
    protected void input(@NonNull JsonElement data)
    {
        super.input(data);
        mResult = parseResult(data);
    }

    public T getResult()
    {
        return mResult;
    }
}
