package com.art.rmr.photocache.ui.fragments;

import android.app.Activity;
import android.support.v4.app.Fragment;

/**
 * Created by art090390 on 28.07.2015.
 */
public abstract class BaseFragment<T> extends Fragment
{
    private T mActivityInterface;

    protected BaseFragment()
    {

    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        try
        {
            mActivityInterface = (T)activity;
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString()
                    + " must implement FeedsHolder");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        mActivityInterface = null;
    }

    protected T getActivityInterface()
    {
        return mActivityInterface;
    }
}
