package com.art.rmr.photocache.controller.operations;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;

import com.art.rmr.photocache.R;
import com.art.rmr.photocache.controller.auth.PhotoCacheAccountConstants;
import com.art.rmr.photocache.controller.auth.PhotoCacheAuthenticator;
import com.redmadrobot.chronos.ChronosOperation;
import com.redmadrobot.chronos.ChronosOperationResult;

import java.io.IOException;

/**
 * Adding account operation.
 */
public class AccountOperation extends ChronosOperation<Bundle>
{
    private static final String PREFS_ACCOUNT = "PREFS_ACCOUNT";

    private Context mContext;
    private AccountManagerFuture<Bundle> mFuture;

    public AccountOperation(@NonNull Activity activity)
    {
        mContext = activity.getApplication();
        String accountType = PhotoCacheAccountConstants.ACCOUNT_TYPE;
        String authTokenType = PhotoCacheAccountConstants.AUTHTOKEN_TYPE_BASE_ACCESS;
        AccountManager manager = AccountManager.get(mContext);
        mFuture = manager.addAccount(accountType, authTokenType, null, null, activity, null, null);
    }

    @Nullable
    @Override
    public Bundle run()
    {
        Bundle result;
        try
        {
            result = mFuture.getResult();
        }
        catch (OperationCanceledException e)
        {
            result = new Bundle();
            result.putInt(AccountManager.KEY_ERROR_CODE, AccountManager.ERROR_CODE_CANCELED);
        }
        catch (AuthenticatorException e)
        {
            result = new Bundle();
            result.putInt(AccountManager.KEY_ERROR_CODE, AccountManager.ERROR_CODE_REMOTE_EXCEPTION);
        }
        catch (IOException e)
        {
            result = new Bundle();
            result.putInt(AccountManager.KEY_ERROR_CODE, AccountManager.ERROR_CODE_NETWORK_ERROR);
            String message = mContext.getResources().getString(R.string.message_network_error);
            result.putString(AccountManager.KEY_ERROR_MESSAGE, message);
        }
        return result;
    }

    @NonNull
    @Override
    public Class<? extends ChronosOperationResult<Bundle>> getResultClass()
    {
        return Result.class;
    }

    public final static class Result extends ChronosOperationResult<Bundle>
    {
    }

    private static SharedPreferences getPreferences(@NonNull Activity activity)
    {
        return activity.getPreferences(Activity.MODE_PRIVATE);
    }

    public static void setLastAccount(@NonNull Activity activity, @NonNull String accountName)
    {
        getPreferences(activity).edit().putString(PREFS_ACCOUNT, accountName).commit();
    }

    @NonNull
    public static Pair<Boolean, Account> getLastAccount(@NonNull Activity activity)
    {
        AccountManager manager = AccountManager.get(activity);
        SharedPreferences prefs = getPreferences(activity);
        if (prefs.contains(PREFS_ACCOUNT))
        {
            Account account = new Account(prefs.getString(PREFS_ACCOUNT, ""), PhotoCacheAccountConstants.ACCOUNT_TYPE);
            if (PhotoCacheAuthenticator.accountExists(manager, account))
            {
                return new Pair<>(true, account);
            }
        }
        return new Pair<>(false, null);
    }

    public static void resetLastAccount(@NonNull Activity activity)
    {
        getPreferences(activity).edit().remove(PREFS_ACCOUNT).commit();
    }
}
