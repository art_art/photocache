package com.art.rmr.photocache.ui.views;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.art.rmr.photocache.R;
import com.art.rmr.photocache.controller.auth.PhotoCacheAccountConstants;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import com.squareup.picasso.Transformation;

/**
 * Created by Артём on 25.07.2015.
 */
public class ToolbarView extends LinearLayout
{
    private Context mContext;
    private Toolbar mToolbar;
    private ImageView mAvatar;
    private TextView mAccount;
    private TabLayout mTabs;

    public ToolbarView(Context context)
    {
        super(context);
        init(context);
    }

    public ToolbarView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init(context);
    }

    public ToolbarView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ToolbarView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes)
    {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context)
    {
        mContext = context;
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.toolbar, this, true);

        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        mAvatar = (ImageView)mToolbar.findViewById(R.id.avatar);
        mAccount = (TextView)mToolbar.findViewById(R.id.account);
        mTabs = (TabLayout)findViewById(R.id.tabs);
    }

    public Toolbar getSupportActionBar()
    {
        return mToolbar;
    }

    public void setUser(Account account)
    {
        mAccount.setText(account.name);

        AccountManager manager = AccountManager.get(mContext);
        String avatar = manager.getUserData(account, PhotoCacheAccountConstants.USER_DATA_AVATAR);
        if (avatar != null)
        {
            Transformation transformation = new RoundedTransformationBuilder()
                    .cornerRadiusDp(mContext.getResources().getDimension(R.dimen.avatar_corner_radius))
                    .oval(false)
                    .build();
            RequestCreator requestCreator = Picasso.with(mContext).load(avatar);
            ViewGroup.LayoutParams layoutParams = mAvatar.getLayoutParams();
            requestCreator.resize(layoutParams.width, layoutParams.height);
            requestCreator.transform(transformation);
            requestCreator.placeholder(R.mipmap.ic_launcher).into(mAvatar);
        }
    }

    public TabLayout getTabs()
    {
        return mTabs;
    }

    public void setBackgroundColor(int color)
    {
        mToolbar.setBackgroundColor(color);
        mTabs.setBackgroundColor(color);
    }
}
