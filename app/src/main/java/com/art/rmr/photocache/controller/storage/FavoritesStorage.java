package com.art.rmr.photocache.controller.storage;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import com.art.rmr.photocache.R;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Артём on 25.07.2015.
 * Базовый класс хранилище изображений.
 */
public abstract class FavoritesStorage
{
    public static final long TIME_STAMP_NO_CHANGES = -1;

    private Set<EventsListener> mListeners;
    private long mChangeTimeStamp;

    protected FavoritesStorage()
    {
        mListeners = new HashSet<>();
        mChangeTimeStamp = TIME_STAMP_NO_CHANGES;
    }

    public synchronized void subscribeEvents(EventsListener listener)
    {
        mListeners.add(listener);
    }

    public synchronized void unsubscribeEvents(EventsListener listener)
    {
        mListeners.remove(listener);
    }

    private void notifyAdded(String name, int position, String uri)
    {
        for (EventsListener listener : mListeners)
            listener.onImageAddedToFavorites(name, position, uri);
    }

    private void notifyRemoved(int position, String name)
    {
        for (EventsListener listener : mListeners)
            listener.onImageRemovedFromFavorites(position, name);
    }

    public synchronized long getChangeTimeStamp()
    {
        return mChangeTimeStamp;
    }

    private synchronized void setChangeTimeStamp()
    {
        mChangeTimeStamp = new Date().getTime();
    }

    protected abstract String remove(int position) throws IOException;

    protected abstract int remove(String name) throws IOException;

    public void removeImageFromFavorites(int position) throws IOException
    {
        String name = remove(position);
        setChangeTimeStamp();
        notifyRemoved(position, name);
    }

    public void removeImageFromFavorites(String name) throws IOException
    {
        int position = remove(name);
        setChangeTimeStamp();
        notifyRemoved(position, name);
    }

    public abstract int getFavoritesCount();

    public abstract FavoriteInfo getFavoriteImage(int position);

    public abstract FavoriteInfo isImageFavorite(String name);

    protected abstract FavoriteInfo saveImage(String name, Bitmap image) throws IOException;

    public interface EventsListener
    {
        void onImageAddedToFavorites(String name, int position, String uri);

        void onImageRemovedFromFavorites(int position, String name);
    }

    public static class FavoriteInfo
    {
        private int mPosition;
        private boolean mIsFavorite;
        private String mUri;
        private String mName;

        public FavoriteInfo()
        {
            mIsFavorite = false;
            mPosition = -1;
        }

        public FavoriteInfo(String uri, int position, String name)
        {
            mName = name;
            mPosition = position;
            mIsFavorite = true;
            mUri = uri;
        }

        public boolean isFavorite()
        {
            return mIsFavorite;
        }

        @Nullable
        public String getUri()
        {
            return mUri;
        }

        public int getPosition()
        {
            return mPosition;
        }

        public String getName()
        {
            return mName;
        }
    }

    public static class SaveResult
    {
        private boolean mResult;
        private Integer mPosition;
        private String mName;
        private String mUri;
        private IOException mIOException;

        public SaveResult(IOException ioException)
        {
            mResult = false;
            mIOException = ioException;
        }

        public SaveResult(String name, int position, String uri)
        {
            mResult = true;
            mName = name;
            mPosition = position;
            mUri = uri;
        }

        public boolean getResult()
        {
            return mResult;
        }

        public Integer getPosition()
        {
            return mPosition;
        }

        public String getName()
        {
            return mName;
        }

        public String getUri()
        {
            return mUri;
        }

        public IOException getIOException()
        {
            return mIOException;
        }
    }

    public static class SaveImageTask extends AsyncTask<Void, Void, SaveResult>
    {
        private FavoritesStorage mFavoritesStorage;
        private String mName;
        private String mUrl;
        private Context mContext;
        private View mFavoriteButton;

        public SaveImageTask(@NonNull Context context, @NonNull FavoritesStorage favoritesStorage, @Nullable View favoriteButton, @NonNull String name, @NonNull String url)
        {
            mContext = context;
            mFavoritesStorage = favoritesStorage;
            mName = name;
            mUrl = url;
            mFavoriteButton = favoriteButton;
        }

        @Override
        protected SaveResult doInBackground(Void... params)
        {
            try
            {
                Bitmap image = Picasso.with(mContext).load(mUrl).get();
                FavoriteInfo favoriteInfo = mFavoritesStorage.saveImage(mName, image);
                return new SaveResult(mName, favoriteInfo.getPosition(), favoriteInfo.getUri());
            }
            catch (IOException e)
            {
                return new SaveResult(e);
            }
        }

        protected void onStart()
        {
            if (mFavoriteButton != null)
                mFavoriteButton.setEnabled(false);
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            onStart();
        }

        protected void onFinished()
        {
            if (mFavoriteButton != null)
                mFavoriteButton.setEnabled(true);
        }

        @Override
        protected void onPostExecute(SaveResult s)
        {
            super.onPostExecute(s);

            if (s.getResult())
            {
                mFavoritesStorage.setChangeTimeStamp();
                mFavoritesStorage.notifyAdded(s.getName(), s.getPosition(), s.getUri());
            }
            else
            {
                Toast.makeText(mContext, R.string.message_saving_file_failed, Toast.LENGTH_SHORT).show();
            }

            onFinished();
        }

        @Override
        protected void onCancelled(SaveResult saveResult)
        {
            super.onCancelled(saveResult);

            onFinished();
        }
    }
}
