package com.art.rmr.photocache.controller.operations;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.graphics.Palette;

import com.art.rmr.photocache.controller.PaletteBroadcastReceiver;
import com.squareup.picasso.RequestCreator;

import java.io.IOException;

/**
 * Created by art090390 on 29.07.2015.
 * Операция получения палитры по фото.
 */
public class GeneratePaletteTask extends AsyncTask<RequestCreator, Void, Palette>
{
    private Context mContext;

    public GeneratePaletteTask(Context context)
    {
        mContext = context;
    }

    @Override
    protected Palette doInBackground(RequestCreator... params)
    {
        try
        {
            Bitmap image = params[0].get();
            Palette.Builder paletteBuilder = Palette.from(image);
            paletteBuilder.maximumColorCount(24);
            return paletteBuilder.generate();
        }
        catch (IOException e)
        {
            return null;
        }
    }

    @Override
    protected void onPostExecute(Palette result)
    {
        super.onPostExecute(result);
        if (result != null)
        {
            Palette.Swatch swatchAccent = result.getVibrantSwatch();
            if (swatchAccent == null)
            {
                swatchAccent = result.getLightVibrantSwatch();
                if (swatchAccent == null)
                    swatchAccent = result.getDarkVibrantSwatch();
            }

            Palette.Swatch swatchPrimaryDark = result.getMutedSwatch();
            if (swatchPrimaryDark == null)
            {
                swatchPrimaryDark = result.getDarkMutedSwatch();
                if (swatchPrimaryDark == null)
                    swatchPrimaryDark = result.getLightMutedSwatch();
            }

            Palette.Swatch swatchPrimary = result.getLightMutedSwatch();
            if (swatchPrimary == null)
            {
                swatchPrimary = result.getDarkMutedSwatch();
            }

            Intent intent = new Intent(PaletteBroadcastReceiver.ACTION_PALETTE_GENERATED);
            if (swatchAccent != null)
                intent.putExtra(PaletteBroadcastReceiver.RESULT_ACCENT, swatchAccent.getRgb());
            if (swatchPrimary != null)
                intent.putExtra(PaletteBroadcastReceiver.RESULT_PRIMARY, swatchPrimary.getRgb());
            if (swatchPrimaryDark != null)
                intent.putExtra(PaletteBroadcastReceiver.RESULT_PRIMARY_DARK, swatchPrimaryDark.getRgb());
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
        }
    }
}
