package com.art.rmr.photocache.controller.auth;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.art.rmr.photocache.ui.activity.AuthenticationActivity;


/**
 * Created by art090390 on 10.07.2015.
 * Унаследованный класс аутентификатора для поддержки аккаунтов.
 */
public class PhotoCacheAuthenticator extends AbstractAccountAuthenticator
{
    private Context mContext;

    public PhotoCacheAuthenticator(Context context)
    {
        super(context);
        mContext = context;
    }

    public static boolean accountExists(@NonNull AccountManager am, @NonNull Account account)
    {
        Account[] accounts = am.getAccountsByType(account.type);
        for (Account account1 : accounts)
            if (account1.name.compareTo(account.name) == 0)
            {
                return true;
            }
        return false;
    }

    @Override
    public Bundle editProperties(AccountAuthenticatorResponse response, String accountType)
    {
        return null;
    }

    @NonNull
    private Intent getAuthActivityIntent(@NonNull AccountAuthenticatorResponse response, @NonNull String authTokenType)
    {
        final Intent intent = new Intent(mContext, AuthenticationActivity.class);
        intent.putExtra(AuthenticationActivity.EXTRA_AUTH_TOKEN_TYPE, authTokenType);
        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        return intent;
    }

    @Override
    @NonNull
    public Bundle addAccount(@NonNull AccountAuthenticatorResponse response, @NonNull String accountType, @NonNull String authTokenType, @NonNull String[] requiredFeatures, @NonNull Bundle options) throws NetworkErrorException
    {
        final Bundle result = new Bundle();
        final Intent intent = getAuthActivityIntent(response, authTokenType);
        intent.putExtra(AuthenticationActivity.EXTRA_CHANGE_ACCOUNT, true);
        result.putParcelable(AccountManager.KEY_INTENT, intent);
        return result;
    }

    @Override
    public Bundle confirmCredentials(AccountAuthenticatorResponse response, Account account, Bundle options) throws NetworkErrorException
    {
        return null;
    }

    @Override
    @NonNull
    public Bundle getAuthToken(@NonNull AccountAuthenticatorResponse response, @NonNull Account account, @NonNull String authTokenType, @NonNull Bundle options) throws NetworkErrorException
    {
        if (!authTokenType.equals(PhotoCacheAccountConstants.AUTHTOKEN_TYPE_BASE_ACCESS))
        {
            final Bundle result = new Bundle();
            result.putInt(AccountManager.KEY_ERROR_CODE, AccountManager.ERROR_CODE_BAD_ARGUMENTS);
            result.putString(AccountManager.KEY_ERROR_MESSAGE, "invalid authTokenType");
            return result;
        }

        final AccountManager am = AccountManager.get(mContext);
        if (accountExists(am, account))
        {
            final String authToken = am.peekAuthToken(account, authTokenType);
            if (authToken != null)
            {
                final Bundle result = new Bundle();
                result.putString(AccountManager.KEY_ACCOUNT_NAME, account.name);
                result.putString(AccountManager.KEY_ACCOUNT_TYPE, account.type);
                result.putString(AccountManager.KEY_AUTHTOKEN, authToken);
                return result;
            }
        }

        Bundle result = new Bundle();
        result.putParcelable(AccountManager.KEY_INTENT, getAuthActivityIntent(response, authTokenType));
        return result;
    }

    @NonNull
    @Override
    public String getAuthTokenLabel(@NonNull String authTokenType)
    {
        if (PhotoCacheAccountConstants.AUTHTOKEN_TYPE_BASE_ACCESS.equals(authTokenType))
        {
            return PhotoCacheAccountConstants.AUTHTOKEN_TYPE_BASE_ACCESS_LABEL;
        }
        else
        {
            return authTokenType + " (Label)";
        }
    }

    @Override
    public Bundle updateCredentials(AccountAuthenticatorResponse response, Account account, String authTokenType, Bundle options) throws NetworkErrorException
    {
        return null;
    }

    @NonNull
    @Override
    public Bundle hasFeatures(AccountAuthenticatorResponse response, Account account, String[] features) throws NetworkErrorException
    {
        final Bundle result = new Bundle();
        result.putBoolean(AccountManager.KEY_BOOLEAN_RESULT, false);
        return result;
    }
}
