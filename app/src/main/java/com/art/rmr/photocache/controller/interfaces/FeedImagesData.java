package com.art.rmr.photocache.controller.interfaces;

import android.view.View;

/**
 * Created by Артём on 26.07.2015.
 */
public interface FeedImagesData
{
    int getImagesCount();
    int getFavoriteButtonImage(int position);
    String getImageUri(int position);
    View.OnClickListener getFavoriteButtonClickListener(int position);
    View.OnClickListener getImageClickListener(int position);
}
