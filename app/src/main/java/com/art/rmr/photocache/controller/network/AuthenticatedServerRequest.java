package com.art.rmr.photocache.controller.network;

import android.net.Uri;
import android.support.annotation.NonNull;

import java.net.MalformedURLException;

/**
 * Created by Артём on 26.07.2015.
 */
public class AuthenticatedServerRequest extends ServerRequest
{
    public static final String LABEL_ACCESS_TOKEN = "access_token";

    private String mToken;

    protected AuthenticatedServerRequest(@NonNull String url, boolean hasOutput, boolean hasInput) throws MalformedURLException
    {
        super(url, hasOutput, hasInput);
    }

    protected AuthenticatedServerRequest(@NonNull Uri.Builder uriBuilder, boolean hasOutput, boolean hasInput, String token) throws MalformedURLException
    {
        super(uriBuilder.appendQueryParameter(LABEL_ACCESS_TOKEN, token).build().toString(), hasOutput, hasInput);
        mToken = token;
    }

    protected String getToken()
    {
        return mToken;
    }
}
