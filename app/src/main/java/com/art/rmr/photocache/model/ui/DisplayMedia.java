package com.art.rmr.photocache.model.ui;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Артём on 25.07.2015.
 * Модель изображения слоя интерфейса.
 */
public class DisplayMedia implements Parcelable
{
    public static final Creator<DisplayMedia> CREATOR = new Creator<DisplayMedia>()
    {
        @Override
        public DisplayMedia createFromParcel(Parcel source)
        {
            return new DisplayMedia(source);
        }

        @Override
        public DisplayMedia[] newArray(int size)
        {
            return new DisplayMedia[size];
        }
    };

    private boolean mIsFavorite;
    private String mUrl;
    private String mName;
    private String mUri;

    private DisplayMedia(Parcel source)
    {
        int isFavorite = source.readInt();
        mIsFavorite = isFavorite == 1;
        mName = source.readString();
        mUrl = source.readString();
        if (isFavorite())
        {
            mUri = source.readString();
        }
    }

    public DisplayMedia(String name, String url)
    {
        mIsFavorite = false;
        mUrl = url;
        mName = name;
    }

    public DisplayMedia(String name, String url, String uri)
    {
        mIsFavorite = true;
        mUrl = url;
        mName = name;
        mUri = uri;
    }

    public boolean isFavorite()
    {
        return mIsFavorite;
    }

    public void setFavorite(boolean isFavorite)
    {
        mIsFavorite = isFavorite;
    }

    public String getUrl()
    {
        return mUrl;
    }

    public String getName()
    {
        return mName;
    }

    public String getUri()
    {
        return mUri;
    }

    public void setUri(String uri)
    {
        mUri = uri;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeInt(isFavorite() ? 1 : 0);
        dest.writeString(getName());
        dest.writeString(getUrl());
        if (isFavorite())
        {
            dest.writeString(getUri());
        }
    }
}
