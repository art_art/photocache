package com.art.rmr.photocache.controller.implementations.instagram;

import android.support.v7.widget.RecyclerView;

import com.art.rmr.photocache.controller.storage.FavoritesStorage;
import com.art.rmr.photocache.model.ui.DisplayMedia;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Артём on 26.07.2015.
 * Класс данные о фото из сети.
 */
public class InstagramFeedData
{
    private List<DisplayMedia> mImages;

    public InstagramFeedData()
    {
        mImages = new ArrayList<>();
    }

    public int getImagesCount()
    {
        return mImages.size();
    }

    public DisplayMedia getImage(int position)
    {
        return mImages.get(position);
    }

    public void addImages(List<DisplayMedia> images)
    {
        mImages.addAll(0, images);
    }

    public void refreshState(FavoritesStorage favoritesStorage, RecyclerView.Adapter adapter)
    {
        for (int i = 0; i < mImages.size(); ++i)
        {
            DisplayMedia media = mImages.get(i);
            FavoritesStorage.FavoriteInfo favoriteInfo = favoritesStorage.isImageFavorite(media.getName());
            boolean isFavorite = favoriteInfo.isFavorite();
            if (media.isFavorite() != isFavorite)
            {
                if (isFavorite)
                {
                    media.setFavorite(true);
                    media.setUri(favoriteInfo.getUri());
                }
                else
                {
                    media.setFavorite(false);
                    media.setUri(null);
                }
                adapter.notifyItemChanged(i);
            }
        }
    }
}
