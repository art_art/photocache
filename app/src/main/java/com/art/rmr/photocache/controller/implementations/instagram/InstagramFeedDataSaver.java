package com.art.rmr.photocache.controller.implementations.instagram;

import android.os.Bundle;
import android.os.Parcelable;

import com.art.rmr.photocache.model.ui.DisplayMedia;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Артём on 26.07.2015.
 * Класс для сохранения и загрузки данных о фото из сети.
 */
public class InstagramFeedDataSaver
{
    private static final String SAVE_IMAGES = "SAVE_IMAGES";

    private InstagramFeedData mFeedData;

    public InstagramFeedDataSaver(InstagramFeedData feedData)
    {
        mFeedData = feedData;
    }

    public void save(Bundle bundle)
    {
        int size = mFeedData.getImagesCount();
        if (size > 0)
        {
            DisplayMedia[] images = new DisplayMedia[size];
            for (int i = 0; i < size; ++i)
            {
                images[i] = mFeedData.getImage(i);
            }
            bundle.putParcelableArray(SAVE_IMAGES, images);
        }
    }

    public void load(Bundle bundle)
    {
        Parcelable[] parcelables = bundle.getParcelableArray(SAVE_IMAGES);
        if (parcelables != null)
        {
            List<DisplayMedia> images = new ArrayList<>();
            for (Parcelable parcelable : parcelables)
            {
                images.add((DisplayMedia)parcelable);
            }
            mFeedData.addImages(images);
        }
    }
}
