package com.art.rmr.photocache.controller.interfaces;

import android.view.View;

import com.art.rmr.photocache.controller.storage.FavoritesStorage;

/**
 * Created by art090390 on 28.07.2015.
 */
public interface FeedActivityInterface
{
    FavoritesStorage getFavoritesStorage();

    View.OnClickListener getImageClickListener(String imageUri, String imageName, int imagePosition, boolean isFavorite, int feed);
}
