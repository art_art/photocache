package com.art.rmr.photocache.ui.activity;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.art.rmr.photocache.R;
import com.art.rmr.photocache.controller.auth.PhotoCacheAccountConstants;
import com.art.rmr.photocache.controller.auth.PhotoCacheAuthenticator;
import com.art.rmr.photocache.controller.network.InstagramServerRequest;
import com.art.rmr.photocache.controller.network.ServerRequest;
import com.art.rmr.photocache.controller.operations.QueryUserDataTask;

import java.util.HashMap;
import java.util.Map;

public class AuthenticationActivity extends AppCompatAccountAuthenticatorActivity
{
    public static final String EXTRA_AUTH_TOKEN_TYPE = "EXTRA_AUTH_TOKEN_TYPE";
    public static final String EXTRA_CHANGE_ACCOUNT = "EXTRA_CHANGE_ACCOUNT";
    private SaveTokenTask mSaveTokenTask;
    private WebView mWebView;
    private WebViewClient mWebViewClient = new WebViewClient()
    {
        @NonNull
        private Map<String, String> parsePairs(@NonNull String pairs)
        {
            Map<String, String> result = new HashMap<>();
            String[] pairsArray = pairs.split("&");
            for (String pair : pairsArray)
            {
                String[] fieldValue = pair.split("=");
                if (fieldValue.length == 2)
                {
                    result.put(fieldValue[0], fieldValue[1]);
                }
            }
            return result;
        }

        private void parseQuery(@NonNull Uri data)
        {
            int errorCode = AccountManager.ERROR_CODE_INVALID_RESPONSE;
            int errorMessage = R.string.authentication_no_fields;
            String query = data.getQuery();
            if (query != null)
            {
                final String FIELD_ERROR = "error";
                final String FIELD_ERROR_REASON = "error_reason";
                final String FIELD_ERROR_DESCRIPTION = "error_description";
                Map<String, String> queryPairs = parsePairs(query);
                boolean containsError = queryPairs.containsKey(FIELD_ERROR);
                boolean containsReason = queryPairs.containsKey(FIELD_ERROR_REASON);
                boolean containDescription = queryPairs.containsKey(FIELD_ERROR_DESCRIPTION);
                if (containsError && containsReason && containDescription)
                {
                    errorCode = AccountManager.ERROR_CODE_INVALID_RESPONSE;
                    errorMessage = R.string.authentication_access_denied;
                }
            }
            finish(RESULT_CANCELED, errorIntent(AuthenticationActivity.this, errorCode, errorMessage));
        }

        private void parseArgs(@NonNull Uri redirect)
        {
            String fragment = redirect.getFragment();
            if (fragment != null)
            {
                Map<String, String> fragmentPairs = parsePairs(fragment);
                final String token = fragmentPairs.get(InstagramServerRequest.LABEL_ACCESS_TOKEN);
                if (token != null && token.length() > 0)
                {
                    mSaveTokenTask.execute(token);
                }
                else
                {
                    parseQuery(redirect);
                }
            }
            else
            {
                parseQuery(redirect);
            }
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url)
        {
            Uri redirect = Uri.parse(url);
            int compareResultScheme = redirect.getScheme().compareTo(getResources().getString(R.string.redirect_uri_scheme));
            int compareResultHost = redirect.getHost().compareTo(getResources().getString(R.string.redirect_uri_host));
            if (compareResultScheme == 0 && compareResultHost == 0)
            {
                mWebView.setVisibility(View.GONE);
                findViewById(R.id.progress).setVisibility(View.VISIBLE);
                parseArgs(redirect);
                return true;
            }
            else
            {
                return super.shouldOverrideUrlLoading(view, url);
            }
        }
    };

    @Override
    @SuppressWarnings("deprecation")
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentication);

        if (!ServerRequest.isNetworkAvailable(this))
        {
            int errorCode = AccountManager.ERROR_CODE_NETWORK_ERROR;
            int errorMessage = R.string.message_network_unavailable;
            finish(RESULT_CANCELED, errorIntent(this, errorCode, errorMessage));
            return;
        }

        mSaveTokenTask = new SaveTokenTask(this);

        final String CLIENT_ID = "b0eeb42edb1b44a2b4115c7d3b4dbf41";
        String url = "https://instagram.com/oauth/authorize/?client_id=" + CLIENT_ID;
        url += "&redirect_uri=" + getResources().getString(R.string.redirect_uri);
        url += "&response_type=token";
        mWebView = (WebView)findViewById(R.id.web);
        if (getIntent().getBooleanExtra(EXTRA_CHANGE_ACCOUNT, false))
        {
            CookieManager cookieManager = CookieManager.getInstance();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            {
                cookieManager.removeAllCookies(null);
            }
            else
            {
                cookieManager.removeAllCookie();
            }
        }
        mWebView.getSettings().setJavaScriptEnabled(false);
        mWebView.setWebViewClient(mWebViewClient);
        mWebView.loadUrl(url);
    }

    @NonNull
    private Intent errorIntent(@NonNull Context context, int errorCode, int errorMessage)
    {
        return errorIntent(errorCode, context.getResources().getString(errorMessage));
    }

    @NonNull
    private Intent errorIntent(int errorCode, @Nullable String errorMessage)
    {
        Intent result = new Intent();
        result.putExtra(AccountManager.KEY_ERROR_CODE, errorCode);
        result.putExtra(AccountManager.KEY_ERROR_MESSAGE, errorMessage);
        return result;
    }

    @Override
    public void onBackPressed()
    {
        mSaveTokenTask.cancel(false);
        int resultCode = RESULT_CANCELED;
        int resultMessage = R.string.authentication_canceled;
        finish(resultCode, errorIntent(this, AccountManager.ERROR_CODE_CANCELED, resultMessage));
    }

    private void finish(int resultCode, @NonNull Intent resultIntent)
    {
        setAccountAuthenticatorResult(resultIntent.getExtras());
        setResult(RESULT_OK, resultIntent);
        finish();
    }

    private void saveToken(@NonNull String userId, @NonNull String userName, @NonNull String token, String avatar)
    {
        AccountManager manager = AccountManager.get(AuthenticationActivity.this);
        Account account = new Account(userName, PhotoCacheAccountConstants.ACCOUNT_TYPE);
        if (!PhotoCacheAuthenticator.accountExists(manager, account))
        {
            Bundle userData = new Bundle();
            userData.putString(PhotoCacheAccountConstants.USER_DATA_ID, userId);
            userData.putString(PhotoCacheAccountConstants.USER_DATA_AVATAR, avatar);
            manager.addAccountExplicitly(account, null, userData);
        }
        manager.setAuthToken(account, PhotoCacheAccountConstants.AUTHTOKEN_TYPE_BASE_ACCESS, token);

        int resultCode = RESULT_OK;
        Intent resultIntent = new Intent();
        resultIntent.putExtra(AccountManager.KEY_ACCOUNT_NAME, account.name);
        resultIntent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, account.type);
        resultIntent.putExtra(AccountManager.KEY_AUTHTOKEN, token);
        finish(resultCode, resultIntent);
    }

    private class SaveTokenTask extends QueryUserDataTask
    {
        public SaveTokenTask(@NonNull Context context)
        {
            super(context);
        }

        @Override
        protected void onError()
        {
            int resultCode = RESULT_CANCELED;
            Intent resultIntent = errorIntent(AccountManager.ERROR_CODE_NETWORK_ERROR, null);
            finish(resultCode, resultIntent);
        }

        @Override
        protected void onSuccess(@NonNull String userId, @NonNull String userName, @NonNull String token, String avatar)
        {
            saveToken(userId, userName, token, avatar);
        }
    }
}
