package com.art.rmr.photocache.ui.fragments.viewing;

import android.content.BroadcastReceiver;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.art.rmr.photocache.R;
import com.art.rmr.photocache.controller.PaletteBroadcastReceiver;
import com.art.rmr.photocache.controller.operations.GeneratePaletteTask;
import com.art.rmr.photocache.utils.ViewColorAnimator;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

/**
 * Created by art090390 on 28.07.2015.
 * Базовый фрагмент просмотра фото.
 */
public abstract class BaseViewFragment extends Fragment
{
    protected static final String ARG_IMAGE_URI = "ARG_IMAGE_URI";
    private static final String ARG_BACKGROUND_COLOR = "ARG_BACKGROUND_COLOR";

    private Integer mImageViewBackgroundColor;
    private String mImageUri;
    private ImageView mImageView;
    private BroadcastReceiver mPaletteReceiver;

    protected BaseViewFragment()
    {
    }

    protected abstract int getLayoutId();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(getLayoutId(), container, false);
        mImageView = (ImageView)rootView.findViewById(R.id.image);
        if (savedInstanceState == null)
        {
            setImage(getArguments().getString(ARG_IMAGE_URI));
        }
        else
        {
            String imageUri = savedInstanceState.getString(ARG_IMAGE_URI);
            if (savedInstanceState.containsKey(ARG_BACKGROUND_COLOR))
            {
                mImageUri = imageUri;
                Picasso.with(getActivity()).load(mImageUri).into(mImageView);
                mImageViewBackgroundColor = savedInstanceState.getInt(ARG_BACKGROUND_COLOR);
                mImageView.setBackgroundColor(mImageViewBackgroundColor);
            }
            else
            {
                setImage(imageUri);
            }
        }
        return rootView;
    }

    protected void setImage(String imageUri)
    {
        mImageUri = imageUri;
        final RequestCreator requestCreator = Picasso.with(getActivity()).load(mImageUri);
        requestCreator.into(mImageView);
        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                new GeneratePaletteTask(getActivity()).execute(requestCreator);
            }
        }, 500);
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putString(ARG_IMAGE_URI, mImageUri);
        if (mImageViewBackgroundColor != null)
        {
            outState.putInt(ARG_BACKGROUND_COLOR, mImageViewBackgroundColor);
        }
    }

    @Override
    public void onStart()
    {
        super.onStart();
        mPaletteReceiver = new PaletteBroadcastReceiver(getActivity())
        {
            @Override
            protected void onColorsReceived(int primary, int primaryDark, int accent)
            {
                mImageViewBackgroundColor = primaryDark;
                new ViewColorAnimator(Color.WHITE, mImageViewBackgroundColor)
                {
                    @Override
                    protected void changeColor(int color)
                    {
                        mImageView.setBackgroundColor(color);
                    }
                }.animate();
            }
        };
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mPaletteReceiver, PaletteBroadcastReceiver.sPaletteFilter);
    }

    @Override
    public void onStop()
    {
        super.onStop();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mPaletteReceiver);
        mPaletteReceiver = null;
    }
}
